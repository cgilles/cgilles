/* ============================================================
 *
 * This file is a part of digiKam project
 * http://www.digikam.org
 *
 * Date        : 2017-09-24
 * Description : a media server to export collections through DLNA.
 *
 * Copyright (C) 2017 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// C++ includes

#include <iostream>
#include <cstdio>

// Qt includes

#include <QUrl>
#include <QDir>
#include <QStringList>
#include <QList>
#include <QDebug>
#include <QApplication>
#include <QFileDialog>
#include <QStandardPaths>

// Local includes

#include "dupnphost.h"

using namespace Digikam;

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    MediaServerMap map;
    QList<QUrl>    list;

/*
    QStringList files = QDir(QString::fromUtf8("/home/gilles/Images")).entryList(QStringList() << QLatin1String("*.jpg"));

    foreach (QString path, files)
        list << QUrl::fromLocalFile(path);
*/

    if (argc <= 1)
    {
        QStringList files = QFileDialog::getOpenFileNames(0, QString::fromLatin1("Select Files to Share With Media Server"),
                                                          QStandardPaths::standardLocations(QStandardPaths::PicturesLocation).first(),
                                                          QLatin1String("Image Files (*.png *.jpg *.tif *.bmp *.gif)"));

        foreach(const QString& f, files)
        {
            list.append(QUrl::fromLocalFile(f));
        }
    }
    else
    {
        for (int i = 1 ; i < argc ; i++)
        {
            list.append(QUrl::fromLocalFile(QString::fromLocal8Bit(argv[i])));
        }
    }

    DUpnpHost* const host = DUpnpHost::instance();

    if (!list.isEmpty())
    {
        host->startServer();
        map.insert(QString::fromUtf8("My Images"), list);
        host->addAlbumsOnServer(map);

        qDebug() << "Press Enter to quit.";

        while (getchar())
        {
            break;
        }
    }

    host->stopServer();
    host->releaseInstance(false);

    return 0;
}
