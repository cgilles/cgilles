/* ============================================================
 *
 * This file is a part of digiKam project
 * http://www.digikam.org
 *
 * Date        : 2017-09-24
 * Description : a media server to export collections through DLNA.
 *
 * Copyright (C) 2017 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "dupnphost.h"

// Qt includes

#include <QDebug>

// Local includes

#include "dmediaserver.h"

namespace Digikam
{

DUpnpHost* DUpnpHost::s_host = NULL;

class DUpnpHostCleaner : public NPT_Thread
{
public:

    explicit DUpnpHostCleaner(DUpnpHost* const host)
        : NPT_Thread(true),
          m_host(host)
    {
    }

    void Run() override
    {
        delete m_host;
    }

    DUpnpHost* m_host;
};

DUpnpHost::DUpnpHost()
{
    m_server = 0;
    m_port   = 0;   // port is selected automaticaly by the server at startup.

    // initialize upnp context
    m_upnp   = new PLT_UPnP();

    // start upnp monitoring
    m_upnp->Start();
}

DUpnpHost::~DUpnpHost()
{
    m_upnp->Stop();
    stopServer();

    delete m_upnp;
}

DUpnpHost* DUpnpHost::instance()
{
    if (!s_host)
    {
        s_host = new DUpnpHost();
    }

    return s_host;
}

void DUpnpHost::releaseInstance(bool bWait)
{
    if (s_host)
    {
        DUpnpHost* const _upnp = s_host;
        s_host                 = NULL;

        if (bWait)
        {
            delete _upnp;
        }
        else
        {
            // since it takes a while to clean up
            // starts a detached thread to do this
            DUpnpHostCleaner* const cleaner = new DUpnpHostCleaner(_upnp);
            cleaner->Start();
        }
    }
}

bool DUpnpHost::isInstantiated()
{
    return (s_host != NULL);
}

bool DUpnpHost::startServer()
{
    if (m_server)
        return false;

    m_server       = new DMediaServer();
    m_server->init(m_port);

    NPT_Result res = m_upnp->AddDevice(m_server->server());

    qDebug() << "Upnp device created:" << res;

    return true;
}

void DUpnpHost::addAlbumsOnServer(const MediaServerMap& map)
{
    m_server->addAlbumsOnServer(map);
}
    
void DUpnpHost::stopServer()
{
    if (!m_server)
        return;

    m_upnp->RemoveDevice(m_server->server());
}

} // namespace Digikam
