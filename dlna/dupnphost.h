/* ============================================================
 *
 * This file is a part of digiKam project
 * http://www.digikam.org
 *
 * Date        : 2017-09-24
 * Description : a media server to export collections through DLNA.
 *
 * Copyright (C) 2017 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef DUPNP_HOST_H
#define DUPNP_HOST_H

// Platinum includes

#include "Platinum.h"

// Local includes

#include "dmediaserver.h"

namespace Digikam
{

class DUpnpHost
{
public:

    DUpnpHost();
    ~DUpnpHost();

    bool              startServer();
    void              stopServer();
    static DUpnpHost* instance();
    static void       releaseInstance(bool bWait);
    static bool       isInstantiated();

    void addAlbumsOnServer(const MediaServerMap& map);
    
private:

    DUpnpHost(const DUpnpHost&)            = delete;
    DUpnpHost& operator=(const DUpnpHost&) = delete;

private:

    int               m_port;
    PLT_UPnP*         m_upnp;
    DMediaServer*     m_server;

    static DUpnpHost* s_host;
};

} // namespace Digikam

#endif // DUPNP_HOST_H
