/*
 *      Copyright (C) 2012-2013 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <utility>
#include <map>

#include <QUrl>
#include <QString>

#include "Platinum/Source/Devices/MediaConnect/PltMediaConnect.h"

class PLT_MediaObject;
class PLT_HttpRequestContext;

namespace UPNP
{

class CUPnPServer : public PLT_MediaConnect,
                    public PLT_FileMediaConnectDelegate
{
public:

    explicit CUPnPServer(const char* friendly_name,
                         const char* uuid = NULL,
                         int         port = 0);
    ~CUPnPServer();

public: // PLT_MediaServer methods

    NPT_Result OnBrowseMetadata(PLT_ActionReference&          action,
                                const char*                   object_id,
                                const char*                   filter,
                                NPT_UInt32                    starting_index,
                                NPT_UInt32                    requested_count,
                                const char*                   sort_criteria,
                                const PLT_HttpRequestContext& context);

    NPT_Result OnBrowseDirectChildren(PLT_ActionReference&          action,
                                      const char*                   object_id,
                                      const char*                   filter,
                                      NPT_UInt32                    starting_index,
                                      NPT_UInt32                    requested_count,
                                      const char*                   sort_criteria,
                                      const PLT_HttpRequestContext& context);

    NPT_Result OnSearchContainer(PLT_ActionReference&          action,
                                 const char*                   container_id,
                                 const char*                   search_criteria,
                                 const char*                   filter,
                                 NPT_UInt32                    starting_index,
                                 NPT_UInt32                    requested_count,
                                 const char*                   sort_criteria,
                                 const PLT_HttpRequestContext& context);

    NPT_Result OnUpdateObject(PLT_ActionReference&             action,
                              const char*                      object_id,
                              NPT_Map<NPT_String,NPT_String>&  current_vals,
                              NPT_Map<NPT_String,NPT_String>&  new_vals,
                              const PLT_HttpRequestContext&    context);

public: // PLT_FileMediaServer methods

    NPT_Result ServeFile(const NPT_HttpRequest&        request,
                         const NPT_HttpRequestContext& context,
                         NPT_HttpResponse&             response,
                         const NPT_String&             file_path);

public: // PLT_DeviceHost methods

    NPT_Result ProcessGetSCPD(PLT_Service*                  service,
                              NPT_HttpRequest&              request,
                              const NPT_HttpRequestContext& context,
                              NPT_HttpResponse&             response);

    NPT_Result SetupServices();
    NPT_Result SetupIcons();

public: // PLT_FileMediaServerDelegate methods

    NPT_String BuildSafeResourceUri(const NPT_HttpUrl& rooturi,
                                    const char*        host,
                                    const char*        file_path);

public: // class methods

    void AddSafeResourceUri(PLT_MediaObject*        object,
                            const NPT_HttpUrl&      rooturi,
                            NPT_List<NPT_IpAddress> ips,
                            const char*             file_path,
                            const NPT_String&       info);

    /* 
     * Samsung's devices get subtitles from header in response (for movie file), not from didl.
     * It's a way to store subtitle uri generated when building didl, to use later in http
     * response
     */
    NPT_Result AddSubtitleUriForSecResponse(NPT_String movie_md5,
                                            NPT_String subtitle_uri);

private: // PLT_MediaServer methods

    void UpdateContainer(const std::string& id);

private: // class methods

    void OnScanCompleted(int type);
    void PropagateUpdates();

    PLT_MediaObject* Build(const QUrl&                   item,
                           bool                          with_count,
                           const PLT_HttpRequestContext& context,
                           NPT_Reference<CThumbLoader>&  thumbLoader,
                           const char*                   parent_id = NULL);

    PLT_MediaObject* BuildObject(const QUrl&                    item,
                                 NPT_String&                    file_path,
                                 bool                           with_count,
                                 NPT_Reference<CThumbLoader>&   thumb_loader,
                                 const PLT_HttpRequestContext*  context = NULL,
                                 CUPnPServer*                   upnp_server = NULL,
                                 UPnPService                    upnp_service = UPnPServiceNone);
/*
    // NOTE: Used by OnSearchContainer()
    NPT_Result BuildResponse(PLT_ActionReference&          action,
                             CFileItemList&                items,
                             const char*                   filter,
                             NPT_UInt32                    starting_index,
                             NPT_UInt32                    requested_count,
                             const char*                   sort_criteria,
                             const PLT_HttpRequestContext& context,
                             const char*                   parent_id);
*/

private: // class methods

/*
    // NOTE: used by OnBrowseDirectChildren()
    static void DefaultSortItems(CFileItemList& items);

    // NOTE: not used.
    static bool SortItems(CFileItemList& items, const char* sort_criteria);
*/

    // NOTE: not used.
    static NPT_String GetParentFolder(NPT_String file_path);

private:

    NPT_Mutex                                              m_CacheMutex;

    NPT_Mutex                                              m_FileMutex;
    NPT_Map<NPT_String, NPT_String>                        m_FileMap;

    std::map<std::string, std::pair<bool, unsigned long> > m_UpdateIDs;
    bool                                                   m_scanning;

public:

    // class members
    static NPT_UInt32                                      m_MaxReturnedItems;
};

} // namespace UPNP
