/*
 *      Copyright (C) 2012-2013 Team XBMC
 *      http://xbmc.org
 *
 *  This Program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This Program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with XBMC; see the file COPYING.  If not, see
 *  <http://www.gnu.org/licenses/>.
 *
 */

#include "Platinum/Source/Platinum/Platinum.h"
#include "dlnaserver.h"

NPT_SET_LOCAL_LOGGER("xbmc.upnp.server")

namespace UPNP
{

NPT_UInt32 CUPnPServer::m_MaxReturnedItems = 0;

const char* audio_containers[] =
{
    "musicdb://genres/",
    "musicdb://artists/",
    "musicdb://albums/",
    "musicdb://songs/",
    "musicdb://recentlyaddedalbums/",
    "musicdb://years/",
    "musicdb://singles/"
};

const char* video_containers[] =
{
    "library://video/movies/titles.xml/",
    "library://video/tvshows/titles.xml/",
    "videodb://recentlyaddedmovies/",
    "videodb://recentlyaddedepisodes/"
};

CUPnPServer::CUPnPServer(const char* friendly_name,
                         const char* uuid,
                         int port)
    : PLT_MediaConnect(friendly_name, false, uuid, port),
      PLT_FileMediaConnectDelegate("/", "/"),
      m_scanning(g_application.IsMusicScanning() || g_application.IsVideoScanning())
{
}

CUPnPServer::~CUPnPServer()
{
}

NPT_Result CUPnPServer::ProcessGetSCPD(PLT_Service*                  service,
                                       NPT_HttpRequest&              request,
                                       const NPT_HttpRequestContext& context,
                                       NPT_HttpResponse&             response)
{
    // needed because PLT_MediaConnect only allows Xbox360 & WMP to search
    return PLT_MediaServer::ProcessGetSCPD(service, request, context, response);
}

NPT_Result CUPnPServer::SetupServices()
{
    PLT_MediaConnect::SetupServices();
    PLT_Service* service = NULL;
    NPT_Result result    = FindServiceById("urn:upnp-org:serviceId:ContentDirectory", service);

    if (service)
    {
        service->SetStateVariable("SearchCapabilities", "upnp:class");
        service->SetStateVariable("SortCapabilities", "res@duration,res@size,res@bitrate,dc:date,dc:title,dc:size,upnp:album,upnp:artist,upnp:albumArtist,upnp:episodeNumber,upnp:genre,upnp:originalTrackNumber,upnp:rating,upnp:episodeCount,upnp:episodeSeason,xbmc:rating,xbmc:dateadded,xbmc:votes");
    }

    m_scanning = true;
    OnScanCompleted(AudioLibrary);
    m_scanning = true;
    OnScanCompleted(VideoLibrary);

    return result;
}

void CUPnPServer::OnScanCompleted(int type)
{
    if (type == AudioLibrary)
    {
        for (size_t i = 0 ; i < ARRAY_SIZE(audio_containers) ; i++)
            UpdateContainer(audio_containers[i]);
    }
    else if (type == VideoLibrary)
    {
        for (size_t i = 0 ; i < ARRAY_SIZE(video_containers) ; i++)
            UpdateContainer(video_containers[i]);
    }
    else
        return;

    m_scanning = false;
    PropagateUpdates();
}

void CUPnPServer::UpdateContainer(const std::string& id)
{
    std::map<std::string, std::pair<bool, unsigned long> >::iterator itr = m_UpdateIDs.find(id);
    unsigned long count = 0;

    if (itr != m_UpdateIDs.end())
        count = ++itr->second.second;

    m_UpdateIDs[id] = std::make_pair(true, count);
    PropagateUpdates();
}

void CUPnPServer::PropagateUpdates()
{
    PLT_Service* service = NULL;
    NPT_String current_ids;
    std::string buffer;
    std::map<std::string, std::pair<bool, unsigned long> >::iterator itr;

    if (m_scanning || !CServiceBroker::GetSettings().GetBool(CSettings::SETTING_SERVICES_UPNPANNOUNCE))
        return;

    NPT_CHECK_LABEL(FindServiceById("urn:upnp-org:serviceId:ContentDirectory", service), failed);

    // we pause, and we must retain any changes which have not been broadcast yet
    NPT_CHECK_LABEL(service->PauseEventing(), failed);
    NPT_CHECK_LABEL(service->GetStateVariableValue("ContainerUpdateIDs", current_ids), failed);
    buffer = (const char*)current_ids;

    if (!buffer.empty())
        buffer.append(",");

    // only broadcast ids with modified bit set
    for (itr = m_UpdateIDs.begin() ; itr != m_UpdateIDs.end() ; ++itr)
    {
        if (itr->second.first)
        {
            buffer.append(StringUtils::Format("%s,%ld,", itr->first.c_str(), itr->second.second).c_str());
            itr->second.first = false;
        }
    }

    // set the value, Platinum will clear ContainerUpdateIDs after sending
    NPT_CHECK_LABEL(service->SetStateVariable("ContainerUpdateIDs", buffer.substr(0,buffer.size()-1).c_str(), true), failed);
    NPT_CHECK_LABEL(service->IncStateVariable("SystemUpdateID"), failed);

    service->PauseEventing(false);
    return;

failed:

    // should attempt to start eventing on a failure
    if (service)
        service->PauseEventing(false);

    CLog::Log(LOGERROR, "UPNP: Unable to propagate updates");
}

NPT_Result CUPnPServer::SetupIcons()
{
    NPT_String file_root = CSpecialProtocol::TranslatePath("special://xbmc/media/").c_str();

    AddIcon(PLT_DeviceIcon("image/png", 256, 256, 8, "/icon256x256.png"), file_root);
    AddIcon(PLT_DeviceIcon("image/png", 120, 120, 8, "/icon120x120.png"), file_root);
    AddIcon(PLT_DeviceIcon("image/png",  48,  48, 8, "/icon48x48.png"),   file_root);
    AddIcon(PLT_DeviceIcon("image/png",  32,  32, 8, "/icon32x32.png"),   file_root);
    AddIcon(PLT_DeviceIcon("image/png",  16,  16, 8, "/icon16x16.png"),   file_root);

    return NPT_SUCCESS;
}

NPT_String CUPnPServer::BuildSafeResourceUri(const NPT_HttpUrl& rooturi,
                                             const char* host,
                                             const char* file_path)
{
    CURL url(file_path);
    std::string md5;

    // determine the filename to provide context to md5'd urls
    std::string filename;

    if (url.IsProtocol("image"))
        filename = URIUtils::GetFileName(url.GetHostName());
    else
        filename = URIUtils::GetFileName(file_path);

    filename = CURL::Encode(filename);
    md5      = XBMC::XBMC_MD5::GetMD5(file_path);
    md5     += "/" + filename;

    {
        NPT_AutoLock lock(m_FileMutex);
        NPT_CHECK(m_FileMap.Put(md5.c_str(), file_path));
    }

    return PLT_FileMediaServer::BuildSafeResourceUri(rooturi, host, md5.c_str());
}

PLT_MediaObject* CUPnPServer::Build(const QUrl&                   item,
                                    bool                          with_count,
                                    const PLT_HttpRequestContext& context,
                                    NPT_Reference<CThumbLoader>&  thumb_loader,
                                    const char*                   parent_id)
{
    PLT_MediaObject* object = NULL;
    NPT_String       path   = item->GetPath().c_str();

    //HACK: temporary disabling count as it thrashes HDD
    with_count = false;

    CLog::Log(LOGDEBUG, "Preparing upnp object for item '%s'", (const char*)path);

    if (path == "virtualpath://upnproot")
    {
        path.TrimRight("/");

        if (path.StartsWith("virtualpath://"))
        {
            object                     = new PLT_MediaContainer;
            object->m_Title            = item->GetLabel().c_str();
            object->m_ObjectClass.type = "object.container";
            object->m_ObjectID         = path;

            // root
            object->m_ObjectID         = "0";
            object->m_ParentID         = "-1";

            // root has 5 children

            //This is dead code because of the HACK a few lines up setting with_count to false
            //if (with_count)
            //{
            //    ((PLT_MediaContainer*)object)->m_ChildrenCount = 5;
            //}
        }
        else
        {
            goto failure;
        }

    }
    else
    {
        // db path handling
        NPT_String file_path, share_name;
        file_path  = item->GetPath().c_str();
        share_name = "";

        if (path.StartsWith("musicdb://")).
        {
            if (path == "musicdb://" )
            {
                item->SetLabel("Music Library");
                item->SetLabelPreformatted(true);
            }
            else
            {
                if (!item->HasMusicInfoTag())
                {
                    MUSICDATABASEDIRECTORY::CQueryParams params;
                    MUSICDATABASEDIRECTORY::CDirectoryNode::GetDatabaseInfo((const char*)path, params);

                    CMusicDatabase db;

                    if (!db.Open() )
                        return NULL;

                    if (params.GetSongId() >= 0 )
                    {
                        CSong song;

                        if (db.GetSong(params.GetSongId(), song))
                            item->GetMusicInfoTag()->SetSong(song);
                    }
                    else if (params.GetAlbumId() >= 0 )
                    {
                        CAlbum album;

                        if (db.GetAlbum(params.GetAlbumId(), album, false))
                            item->GetMusicInfoTag()->SetAlbum(album);
                    }
                    else if (params.GetArtistId() >= 0 )
                    {
                        CArtist artist;

                        if (db.GetArtist(params.GetArtistId(), artist, false))
                            item->GetMusicInfoTag()->SetArtist(artist);
                    }
                }

                if (item->GetLabel().empty())
                {
                    // if no label try to grab it from node type
                    std::string label;

                    if (CMusicDatabaseDirectory::GetLabel((const char*)path, label))
                    {
                        item->SetLabel(label);
                        item->SetLabelPreformatted(true);
                    }
                }
            }
        }
        else if (file_path.StartsWith("library://") || file_path.StartsWith("videodb://"))
        {
            if (path == "library://video/" )
            {
                item->SetLabel("Video Library");
                item->SetLabelPreformatted(true);
            }
            else
            {
                if (!item->HasVideoInfoTag())
                {
                    VIDEODATABASEDIRECTORY::CQueryParams params;
                    VIDEODATABASEDIRECTORY::CDirectoryNode::GetDatabaseInfo((const char*)path, params);

                    CVideoDatabase db;

                    if (!db.Open())
                        return NULL;

                    if      (params.GetMovieId() >= 0)
                        db.GetMovieInfo((const char*)path, *item->GetVideoInfoTag(), params.GetMovieId());
                    else if (params.GetMVideoId() >= 0)
                        db.GetMusicVideoInfo((const char*)path, *item->GetVideoInfoTag(), params.GetMVideoId());
                    else if (params.GetEpisodeId() >= 0)
                        db.GetEpisodeInfo((const char*)path, *item->GetVideoInfoTag(), params.GetEpisodeId());
                    else if (params.GetTvShowId() >= 0)
                        db.GetTvShowInfo((const char*)path, *item->GetVideoInfoTag(), params.GetTvShowId());
                }

                if (item->GetVideoInfoTag()->m_type == MediaTypeTvShow ||
                    item->GetVideoInfoTag()->m_type == MediaTypeSeason)
                    {
                        // for tvshows and seasons, iEpisode and playCount are invalid
                        item->GetVideoInfoTag()->m_iEpisode = (int)item->GetProperty("totalepisodes").asInteger();
                        item->GetVideoInfoTag()->SetPlayCount(static_cast<int>(item->GetProperty("watchedepisodes").asInteger()));
                }

                // try to grab title from tag
                if (item->HasVideoInfoTag() && !item->GetVideoInfoTag()->m_strTitle.empty())
                {
                    item->SetLabel(item->GetVideoInfoTag()->m_strTitle);
                    item->SetLabelPreformatted(true);
                }

                // try to grab it from the folder
                if (item->GetLabel().empty())
                {
                    std::string label;

                    if (CVideoDatabaseDirectory::GetLabel((const char*)path, label))
                    {
                        item->SetLabel(label);
                        item->SetLabelPreformatted(true);
                    }
                }
            }
        }

        // not a virtual path directory, new system
        object = BuildObject(*item.get(),
                             file_path,
                             with_count,
                             thumb_loader,
                             &context,
                             this,
                             UPnPContentDirectory);

        // set parent id if passed, otherwise it should have been determined
        if (object && parent_id)
        {
            object->m_ParentID = parent_id;
        }
    }

    if (object)
    {
        // remap Root virtualpath://upnproot/ to id "0"
        if (object->m_ObjectID == "virtualpath://upnproot/")
            object->m_ObjectID = "0";

        // remap Parent Root virtualpath://upnproot/ to id "0"
        if (object->m_ParentID == "virtualpath://upnproot/")
            object->m_ParentID = "0";
    }

    return object;

failure:

    delete object;
    return NULL;
}

PLT_MediaObject* BuildObject(const QUrl&                   item,
                             NPT_String&                   file_path,
                             bool                          with_count,
                             NPT_Reference<CThumbLoader>&  thumb_loader,
                             const PLT_HttpRequestContext* context,
                             CUPnPServer*                  upnp_server,
                             UPnPService                   upnp_service)
{
    PLT_MediaItemResource resource;
    PLT_MediaObject*      object = NULL;
    std::string           thumb;

    CLog::Log(LOGDEBUG, "UPnP: Building didl for object '%s'", item.GetPath().c_str());

    EClientQuirks quirks = GetClientQuirks(context);

    // get list of ip addresses
    NPT_List<NPT_IpAddress> ips;
    NPT_HttpUrl             rooturi;
    NPT_CHECK_LABEL(PLT_UPnPMessageHelper::GetIPAddresses(ips), failure);

    // if we're passed an interface where we received the request from
    // move the ip to the top
    if (context && context->GetLocalAddress().GetIpAddress().ToString() != "0.0.0.0")
    {
        rooturi = NPT_HttpUrl(context->GetLocalAddress().GetIpAddress().ToString(),
                              context->GetLocalAddress().GetPort(), "/");
        ips.Remove(context->GetLocalAddress().GetIpAddress());
        ips.Insert(ips.GetFirstItem(), context->GetLocalAddress().GetIpAddress());
    }
    else if(upnp_server)
    {
        rooturi = NPT_HttpUrl("localhost", upnp_server->GetPort(), "/");
    }

    if (!item.m_bIsFolder)
    {
        object             = new PLT_MediaItem();
        object->m_ObjectID = item.GetPath().c_str();

        /* Setup object type */

        if (item.IsMusicDb() || item.IsAudio())
        {
            object->m_ObjectClass.type = "object.item.audioItem.musicTrack";

            if (item.HasMusicInfoTag())
            {
                CMusicInfoTag* tag = (CMusicInfoTag*)item.GetMusicInfoTag();
                PopulateObjectFromTag(*tag, *object, &file_path, &resource, quirks, upnp_service);
            }
        }
        else if (item.IsVideoDb() || item.IsVideo())
        {
            object->m_ObjectClass.type = "object.item.videoItem";

            if(quirks & ECLIENTQUIRKS_UNKNOWNSERIES)
                object->m_Affiliation.album = "[Unknown Series]";

            if (item.HasVideoInfoTag())
            {
                CVideoInfoTag* tag = (CVideoInfoTag*)item.GetVideoInfoTag();
                PopulateObjectFromTag(*tag, *object, &file_path, &resource, quirks, upnp_service);
            }
        }
        else if (item.IsPicture())
        {
            object->m_ObjectClass.type = "object.item.imageItem.photo";
        }
        else
        {
            object->m_ObjectClass.type = "object.item";
        }

        // duration of zero is invalid
        if (resource.m_Duration == 0)
            resource.m_Duration = -1;

        // Set the resource file size
        resource.m_Size = item.m_dwSize;

        if(resource.m_Size == 0)
            resource.m_Size = (NPT_LargeSize)-1;

        // set date
        if (object->m_Date.IsEmpty() && item.m_dateTime.IsValid())
        {
            object->m_Date = item.m_dateTime.GetAsW3CDate().c_str();
        }

        if (upnp_server)
        {
            upnp_server->AddSafeResourceUri(object, rooturi, ips, file_path, GetProtocolInfo(item, "http", context));
        }

        // if the item is remote, add a direct link to the item
        if (URIUtils::IsRemote((const char*)file_path))
        {
            resource.m_ProtocolInfo = PLT_ProtocolInfo(GetProtocolInfo(item, item.GetURL().GetProtocol().c_str(), context));
            resource.m_Uri          = file_path;

            // if the direct link can be served directly using http, then push it in front
            // otherwise keep the xbmc-get resource last and let a compatible client look for it
            if (resource.m_ProtocolInfo.ToString().StartsWith("xbmc", true))
            {
                object->m_Resources.Add(resource);
            }
            else
            {
                object->m_Resources.Insert(object->m_Resources.GetFirstItem(), resource);
            }
        }

        // copy across the known metadata
        for(unsigned i=0; i<object->m_Resources.GetItemCount(); i++)
        {
            object->m_Resources[i].m_Size       = resource.m_Size;
            object->m_Resources[i].m_Duration   = resource.m_Duration;
            object->m_Resources[i].m_Resolution = resource.m_Resolution;
        }

        // Some upnp clients expect all audio items to have parent root id 4
#ifdef WMP_ID_MAPPING
        object->m_ParentID = "4";
#endif
    }
    else
    {
        PLT_MediaContainer* container = new PLT_MediaContainer;
        object                        = container;

        /* Assign a title and id for this container */
        container->m_ObjectID         = item.GetPath().c_str();
        container->m_ObjectClass.type = "object.container";
        container->m_ChildrenCount    = -1;

        /* this might be overkill, but hey */
        if (item.IsMusicDb())
        {
            MUSICDATABASEDIRECTORY::NODE_TYPE node = CMusicDatabaseDirectory::GetDirectoryType(item.GetPath());

            switch(node)
            {
                case MUSICDATABASEDIRECTORY::NODE_TYPE_ARTIST:
                {
                    container->m_ObjectClass.type += ".person.musicArtist";
                    CMusicInfoTag *tag             = (CMusicInfoTag*)item.GetMusicInfoTag();

                    if (tag)
                    {
                        container->m_People.artists.Add(
                            CorrectAllItemsSortHack(tag->GetArtistString()).c_str(), "Performer");
                        container->m_People.artists.Add(
                            CorrectAllItemsSortHack((!tag->GetAlbumArtistString().empty() ? tag->GetAlbumArtistString() : tag->GetArtistString())).c_str(), "AlbumArtist");
                    }
#ifdef WMP_ID_MAPPING
                    // Some upnp clients expect all artists to have parent root id 107
                    container->m_ParentID = "107";
#endif
                }
                break;

                case MUSICDATABASEDIRECTORY::NODE_TYPE_ALBUM:
                case MUSICDATABASEDIRECTORY::NODE_TYPE_ALBUM_COMPILATIONS:
                case MUSICDATABASEDIRECTORY::NODE_TYPE_ALBUM_RECENTLY_ADDED:
                case MUSICDATABASEDIRECTORY::NODE_TYPE_YEAR_ALBUM:
                {
                    container->m_ObjectClass.type += ".album.musicAlbum";
                    // for Sonos to be happy
                    CMusicInfoTag *tag = (CMusicInfoTag*)item.GetMusicInfoTag();

                    if (tag)
                    {
                        container->m_People.artists.Add(
                            CorrectAllItemsSortHack(tag->GetArtistString()).c_str(), "Performer");
                        container->m_People.artists.Add(
                            CorrectAllItemsSortHack(!tag->GetAlbumArtistString().empty() ? tag->GetAlbumArtistString() : tag->GetArtistString()).c_str(), "AlbumArtist");
                        container->m_Affiliation.album = CorrectAllItemsSortHack(tag->GetAlbum()).c_str();
                    }
#ifdef WMP_ID_MAPPING
                    // Some upnp clients expect all albums to have parent root id 7
                    container->m_ParentID = "7";
#endif
                }
                break;

                case MUSICDATABASEDIRECTORY::NODE_TYPE_GENRE:
                    container->m_ObjectClass.type += ".genre.musicGenre";
                    break;
                default:
                    break;
            }
        }
        else if (item.IsVideoDb())
        {
            VIDEODATABASEDIRECTORY::NODE_TYPE node = CVideoDatabaseDirectory::GetDirectoryType(item.GetPath());
            CVideoInfoTag &tag                     = *(CVideoInfoTag*)item.GetVideoInfoTag();

            switch(node)
            {
                case VIDEODATABASEDIRECTORY::NODE_TYPE_GENRE:
                    container->m_ObjectClass.type += ".genre.movieGenre";
                    break;
                case VIDEODATABASEDIRECTORY::NODE_TYPE_ACTOR:
                    container->m_ObjectClass.type += ".person.videoArtist";
                    container->m_Creator           = StringUtils::Join(tag.m_artist, g_advancedSettings.m_videoItemSeparator).c_str();
                    container->m_Title             = tag.m_strTitle.c_str();
                    break;
                case VIDEODATABASEDIRECTORY::NODE_TYPE_SEASONS:
                case VIDEODATABASEDIRECTORY::NODE_TYPE_TITLE_TVSHOWS:
                    container->m_ObjectClass.type       += ".album.videoAlbum";
                    container->m_Recorded.series_title   = tag.m_strShowTitle.c_str();
                    container->m_Recorded.episode_number = tag.m_iEpisode;
                    container->m_MiscInfo.play_count     = tag.GetPlayCount();
                    container->m_Title                   = tag.m_strTitle.c_str();
                    container->m_Date                    = tag.GetPremiered().GetAsW3CDate().c_str();

                    for (unsigned int index = 0 ; index < tag.m_genre.size() ; index++)
                        container->m_Affiliation.genres.Add(tag.m_genre.at(index).c_str());

                    for(CVideoInfoTag::iCast it = tag.m_cast.begin() ; it != tag.m_cast.end() ; it++)
                    {
                        container->m_People.actors.Add(it->strName.c_str(), it->strRole.c_str());
                    }

                    for (unsigned int index = 0 ; index < tag.m_director.size() ; index++)
                            container->m_People.directors.Add(tag.m_director[index].c_str());

                    for (unsigned int index = 0 ; index < tag.m_writingCredits.size() ; index++)
                        container->m_People.authors.Add(tag.m_writingCredits[index].c_str());

                    container->m_Description.description      = tag.m_strTagLine.c_str();
                    container->m_Description.long_description = tag.m_strPlot.c_str();

                    break;
                default:
                    container->m_ObjectClass.type += ".storageFolder";
                    break;
            }
        }
        else if (item.IsPlayList() || item.IsSmartPlayList())
        {
            container->m_ObjectClass.type += ".playlistContainer";
        }

        if(quirks & ECLIENTQUIRKS_ONLYSTORAGEFOLDER)
        {
            container->m_ObjectClass.type = "object.container.storageFolder";
        }

        /* Get the number of children for this container */
        if (with_count && upnp_server)
        {
            if (object->m_ObjectID.StartsWith("virtualpath://"))
            {
                NPT_LargeSize count        = 0;
                NPT_CHECK_LABEL(NPT_File::GetSize(file_path, count), failure);
                container->m_ChildrenCount = (NPT_Int32)count;
            }
            else
            {
                /* this should be a standard path */
                //! @todo - get file count of this directory
            }
        }
    }

    // set a title for the object
    if (object->m_Title.IsEmpty())
    {
        if (!item.GetLabel().empty())
        {
            std::string title = item.GetLabel();

            if (item.IsPlayList() || !item.m_bIsFolder)
                URIUtils::RemoveExtension(title);

            object->m_Title = title.c_str();
        }
    }

    if (upnp_server)
    {
        // determine the correct artwork for this item
        if (!thumb_loader.IsNull())
            thumb_loader->LoadItem(&item);

        // finally apply the found artwork
        thumb = item.GetArt("thumb");

        if (!thumb.empty())
        {
            PLT_AlbumArtInfo art;
            art.uri = upnp_server->BuildSafeResourceUri(rooturi,
                                                        (*ips.GetFirstItem()).ToString(),
                                                        CTextureUtils::GetWrappedImageURL(thumb).c_str());

            // Set DLNA profileID by extension, defaulting to JPEG.

            if (URIUtils::HasExtension(thumb, ".png"))
            {
                art.dlna_profile = "PNG_TN";
            }
            else
            {
                art.dlna_profile = "JPEG_TN";
            }

            object->m_ExtraInfo.album_arts.Add(art);
        }

        for (CGUIListItem::ArtMap::const_iterator itArtwork = item.GetArt().begin() ;
             itArtwork != item.GetArt().end() ; ++itArtwork)
        {
            if (!itArtwork->first.empty() && !itArtwork->second.empty())
            {
                std::string wrappedUrl = CTextureUtils::GetWrappedImageURL(itArtwork->second);
                object->m_XbmcInfo.artwork.Add(itArtwork->first.c_str(),
                                               upnp_server->BuildSafeResourceUri(rooturi,
                                                    (*ips.GetFirstItem()).ToString(),
                                                    wrappedUrl.c_str()));

                upnp_server->AddSafeResourceUri(object,
                                                rooturi,
                                                ips,
                                                wrappedUrl.c_str(),
                                                ("xbmc.org:*:" + itArtwork->first + ":*").c_str());
            }
        }
    }

    // look for and add external subtitle if we are processing a video file and
    // we are being called by a UPnP player or renderer or the user has chosen
    // to look for external subtitles
    if (upnp_server != NULL && item.IsVideo() &&
       (upnp_service == UPnPPlayer || upnp_service == UPnPRenderer ||
        CServiceBroker::GetSettings().GetBool(CSettings::SETTING_SERVICES_UPNPLOOKFOREXTERNALSUBTITLES)))
    {
        // find any available external subtitles
        std::vector<std::string> filenames;
        std::vector<std::string> subtitles;
        CUtil::ScanForExternalSubtitles(file_path.GetChars(), filenames);
        std::string ext;

        for (unsigned int i = 0 ; i < filenames.size() ; i++)
        {
            ext = URIUtils::GetExtension(filenames[i]).c_str();
            ext = ext.substr(1);
            std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

            /* Hardcoded check for extension is not the best way, but it can't be allowed to pass all
               subtitle extension (ex. rar or zip). There are the most popular extensions support by UPnP devices.*/
            if (ext == "txt" ||
                ext == "srt" ||
                ext == "ssa" ||
                ext == "ass" ||
                ext == "sub" ||
                ext == "smi")
            {
                subtitles.push_back(filenames[i]);
            }
        }

        std::string subtitlePath;

        if (subtitles.size() == 1)
        {
            subtitlePath = subtitles[0];
        }
        else if (!subtitles.empty())
        {
            /* trying to find subtitle with prefered language settings */
            std::string preferredLanguage = (CServiceBroker::GetSettings().GetSetting("locale.subtitlelanguage"))->ToString();
            std::string preferredLanguageCode;
            g_LangCodeExpander.ConvertToISO6392B(preferredLanguage, preferredLanguageCode);

            for (unsigned int i = 0 ; i < subtitles.size() ; i++)
            {
                ExternalStreamInfo info = CUtil::GetExternalStreamDetailsFromFilename(file_path.GetChars(), subtitles[i]);

                if (preferredLanguageCode == info.language)
                {
                    subtitlePath = subtitles[i];
                    break;
                }
            }

            /* if not found subtitle with prefered language, get the first one */
            if (subtitlePath.empty())
            {
                subtitlePath = subtitles[0];
            }
        }

        if (!subtitlePath.empty())
        {
            /* subtitles are added as 2 resources, 2 sec resources and 1 addon to video resource, to be compatible with
               the most of the devices; all UPnP devices take the last one it could handle,
               and skip ones it doesn't "understand" */
            // add subtitle resource with standard protocolInfo
            NPT_String protocolInfo   = GetProtocolInfo(CFileItem(subtitlePath, false), "http", context);
            upnp_server->AddSafeResourceUri(object, rooturi, ips, NPT_String(subtitlePath.c_str()), protocolInfo);

            // add subtitle resource with smi/caption protocol info (some devices)
            PLT_ProtocolInfo protInfo = PLT_ProtocolInfo(protocolInfo);
            protocolInfo              = protInfo.GetProtocol() +
                                        ":" + protInfo.GetMask() +
                                        ":smi/caption:" + protInfo.GetExtra();

            upnp_server->AddSafeResourceUri(object, rooturi, ips, NPT_String(subtitlePath.c_str()), protocolInfo);

            ext = URIUtils::GetExtension(subtitlePath).c_str();
            ext = ext.substr(1);
            std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

            NPT_String subtitle_uri    = object->m_Resources[object->m_Resources.GetItemCount() - 1].m_Uri;

            // add subtitle to video resource (the first one) (for some devices)
            object->m_Resources[0].m_CustomData["xmlns:pv"]            = "http://www.pv.com/pvns/";
            object->m_Resources[0].m_CustomData["pv:subtitleFileUri"]  = subtitle_uri;
            object->m_Resources[0].m_CustomData["pv:subtitleFileType"] = ext.c_str();

            // for samsung devices
            PLT_SecResource sec_res;
            sec_res.name               = "CaptionInfoEx";
            sec_res.value              = subtitle_uri;
            sec_res.attributes["type"] = ext.c_str();
            object->m_SecResources.Add(sec_res);
            sec_res.name               = "CaptionInfo";
            object->m_SecResources.Add(sec_res);

            // adding subtitle uri for movie md5, for later use in http response
            NPT_String movie_md5       = object->m_Resources[0].m_Uri;
            movie_md5                  = movie_md5.Right(movie_md5.GetLength() - movie_md5.Find("/%25/") - 5);
            upnp_server->AddSubtitleUriForSecResponse(movie_md5, subtitle_uri);
        }
    }

    return object;

failure:

    delete object;
    return NULL;
}

void CUPnPServer::Announce(AnnouncementFlag flag,
                           const char*      sender,
                           const char*      message,
                           const CVariant&  data)
{
    NPT_String  path;
    int         item_id;
    std::string item_type;

    if (strcmp(sender, "xbmc"))
        return;

    if (strcmp(message, "OnUpdate")      && strcmp(message, "OnRemove") &&
        strcmp(message, "OnScanStarted") && strcmp(message, "OnScanFinished"))
        return;

    if (data.isNull())
    {
        if (!strcmp(message, "OnScanStarted") || !strcmp(message, "OnCleanStarted"))
        {
            m_scanning = true;
        }
        else if (!strcmp(message, "OnScanFinished") || !strcmp(message, "OnCleanFinished"))
        {
            OnScanCompleted(flag);
        }
    }
    else
    {
        // handle both updates & removals
        if (!data["item"].isNull())
        {
            item_id   = (int)data["item"]["id"].asInteger();
            item_type = data["item"]["type"].asString();
        }
        else
        {
            item_id   = (int)data["id"].asInteger();
            item_type = data["type"].asString();
        }

        // we always update 'recently added' nodes along with the specific container,
        // as we don't differentiate 'updates' from 'adds' in RPC interface
        if (flag == VideoLibrary)
        {
            if (item_type == MediaTypeEpisode)
            {
                CVideoDatabase db;

                if (!db.Open())
                    return;

                int show_id   = db.GetTvShowForEpisode(item_id);
                int season_id = db.GetSeasonForEpisode(item_id);
                UpdateContainer(StringUtils::Format("videodb://tvshows/titles/%d/", show_id));
                UpdateContainer(StringUtils::Format("videodb://tvshows/titles/%d/%d/?tvshowid=%d",
                                                    show_id, season_id, show_id));
                UpdateContainer("videodb://recentlyaddedepisodes/");
            }
            else if (item_type == MediaTypeTvShow)
            {
                UpdateContainer("library://video/tvshows/titles.xml/");
                UpdateContainer("videodb://recentlyaddedepisodes/");
            }
            else if (item_type == MediaTypeMovie)
            {
                UpdateContainer("library://video/movies/titles.xml/");
                UpdateContainer("videodb://recentlyaddedmovies/");
            }
            else if (item_type == MediaTypeMusicVideo)
            {
                UpdateContainer("library://video/musicvideos/titles.xml/");
                UpdateContainer("videodb://recentlyaddedmusicvideos/");
            }
        }
        else if (flag == AudioLibrary && item_type == MediaTypeSong)
        {
            // we also update the 'songs' container is maybe a performance drop too
            // high? would need to check if slow clients even cache at all anyway
            CMusicDatabase db;
            CAlbum         album;

            if (!db.Open())
                return;

            if (db.GetAlbumFromSong(item_id, album))
            {
                UpdateContainer(StringUtils::Format("musicdb://albums/%ld", album.idAlbum));
                UpdateContainer("musicdb://songs/");
                UpdateContainer("musicdb://recentlyaddedalbums/");
            }
        }
    }
}

static NPT_String TranslateWMPObjectId(NPT_String id)
{
    if (id == "0")
    {
        id = "virtualpath://upnproot/";
    }
    else if (id == "15")
    {
        // Xbox 360 asking for videos
        id = "library://video/";
    }
    else if (id == "16")
    {
        // Xbox 360 asking for photos
    }
    else if (id == "107")
    {
        // Sonos uses 107 for artists root container id
        id = "musicdb://artists/";
    }
    else if (id == "7")
    {
        // Sonos uses 7 for albums root container id
        id = "musicdb://albums/";
    }
    else if (id == "4")
    {
        // Sonos uses 4 for tracks root container id
        id = "musicdb://songs/";
    }

    CLog::Log(LOGDEBUG, "UPnP Translated id to '%s'", (const char*)id);
    return id;
}

NPT_Result ObjectIDValidate(const NPT_String& id)
{
    if (CFileUtils::RemoteAccessAllowed(id.GetChars()))
        return NPT_SUCCESS;

    return NPT_ERROR_NO_SUCH_FILE;
}

NPT_Result CUPnPServer::OnBrowseMetadata(PLT_ActionReference&          action,
                                         const char*                   object_id,
                                         const char*                   filter,
                                         NPT_UInt32                    starting_index,
                                         NPT_UInt32                    requested_count,
                                         const char*                   sort_criteria,
                                         const PLT_HttpRequestContext& context)
{
    NPT_COMPILER_UNUSED(sort_criteria);
    NPT_COMPILER_UNUSED(requested_count);
    NPT_COMPILER_UNUSED(starting_index);

    NPT_String                     didl;
    NPT_Reference<PLT_MediaObject> object;
    NPT_String                     id = TranslateWMPObjectId(object_id);
    CFileItemPtr                   item;
    NPT_Reference<CThumbLoader>    thumb_loader;

    CLog::Log(LOGINFO, "Received UPnP Browse Metadata request for object '%s'",
              (const char*)object_id);

    if (NPT_FAILED(ObjectIDValidate(id)))
    {
        action->SetError(701, "Incorrect ObjectID.");
        return NPT_FAILURE;
    }

    if (id.StartsWith("virtualpath://"))
    {
        id.TrimRight("/");

        if (id == "virtualpath://upnproot")
        {
            id                += "/";
            item.reset(new CFileItem((const char*)id, true));
            item->SetLabel("Root");
            item->SetLabelPreformatted(true);
            object             = Build(item, true, context, thumb_loader);
            object->m_ParentID = "-1";
        }
        else
        {
            return NPT_FAILURE;
        }
    }
    else
    {
        // determine if it's a container by calling CDirectory::Exists
        item.reset(new CFileItem((const char*)id, CDirectory::Exists((const char*)id)));

        // attempt to determine the parent of this item
        std::string parent;

        if (URIUtils::IsVideoDb((const char*)id) ||
            URIUtils::IsMusicDb((const char*)id) ||
            StringUtils::StartsWithNoCase((const char*)id, "library://video/"))
        {
            if (!URIUtils::GetParentPath((const char*)id, parent))
            {
                parent = "0";
            }
        }
        else
        {
            // non-library objects - playlists / sources
            //
            // we could instead store the parents in a hash during every browse
            // or could handle this in URIUtils::GetParentPath() possibly,
            // however this is quicker to implement and subsequently purge when a
            // better solution presents itself
            std::string child_id((const char*)id);

            if      (StringUtils::StartsWithNoCase(child_id, "special://musicplaylists/"))
                parent = "musicdb://";
            else if (StringUtils::StartsWithNoCase(child_id, "special://videoplaylists/"))
                parent = "library://video/";
            else if (StringUtils::StartsWithNoCase(child_id, "sources://video/"))
                parent = "library://video/";
            else if (StringUtils::StartsWithNoCase(child_id, "special://profile/playlists/music/"))
                parent = "special://musicplaylists/";
            else if (StringUtils::StartsWithNoCase(child_id, "special://profile/playlists/video/"))
                parent = "special://videoplaylists/";
            else
                parent = "sources://video/"; // this can only match video sources
        }

        if (item->IsVideoDb())
        {
            thumb_loader = NPT_Reference<CThumbLoader>(new CVideoThumbLoader());
        }
        else if (item->IsMusicDb())
        {
            thumb_loader = NPT_Reference<CThumbLoader>(new CMusicThumbLoader());
        }

        if (!thumb_loader.IsNull())
        {
            thumb_loader->OnLoaderStart();
        }

        object = Build(item, true, context, thumb_loader,
                       parent.empty() ? NULL : parent.c_str());
    }

    if (object.IsNull())
    {
        /* error */
        NPT_LOG_WARNING_1("CUPnPServer::OnBrowseMetadata - Object null (%s)", object_id);
        action->SetError(701, "No Such Object.");
        return NPT_FAILURE;
    }

    NPT_String tmp;
    NPT_CHECK(PLT_Didl::ToDidl(*object.AsPointer(), filter, tmp));

    /* add didl header and footer */
    didl = didl_header + tmp + didl_footer;

    NPT_CHECK(action->SetArgumentValue("Result",         didl));
    NPT_CHECK(action->SetArgumentValue("NumberReturned", "1"));
    NPT_CHECK(action->SetArgumentValue("TotalMatches",   "1"));

    // update ID may be wrong here, it should be the one of the container?
    NPT_CHECK(action->SetArgumentValue("UpdateId",       "0"));

    //! @todo We need to keep track of the overall SystemUpdateID of the CDS

    return NPT_SUCCESS;
}

NPT_Result CUPnPServer::OnBrowseDirectChildren(PLT_ActionReference&          action,
                                               const char*                   object_id,
                                               const char*                   filter,
                                               NPT_UInt32                    starting_index,
                                               NPT_UInt32                    requested_count,
                                               const char*                   sort_criteria,
                                               const PLT_HttpRequestContext& context)
{
    CFileItemList items;
    NPT_String    parent_id = TranslateWMPObjectId(object_id);

    CLog::Log(LOGINFO,
              "UPnP: Received Browse DirectChildren request for object '%s', with sort criteria %s",
              object_id,
              sort_criteria);

    if (NPT_FAILED(ObjectIDValidate(parent_id)))
    {
        action->SetError(701, "Incorrect ObjectID.");
        return NPT_FAILURE;
    }

    items.SetPath(std::string(parent_id));

    // guard against loading while saving to the same cache file
    // as CArchive currently performs no locking itself
    bool load;

    {
        NPT_AutoLock lock(m_CacheMutex);
        load = items.Load();
    }

    if (!load)
    {
        // cache anything that takes more than a second to retrieve
        unsigned int time = XbmcThreads::SystemClockMillis();

        if (parent_id.StartsWith("virtualpath://upnproot"))
        {
            CFileItemPtr item;

            // music library
            item.reset(new CFileItem("musicdb://", true));
            item->SetLabel("Music Library");
            item->SetLabelPreformatted(true);
            items.Add(item);

            // video library
            item.reset(new CFileItem("library://video/", true));
            item->SetLabel("Video Library");
            item->SetLabelPreformatted(true);
            items.Add(item);

            items.Sort(SortByLabel, SortOrderAscending);
        }
        else
        {
            // this is the only way to hide unplayable items in the 'files'
            // view as we cannot tell what context (eg music vs video) the
            // request came from
            std::string supported = g_advancedSettings.GetPictureExtensions() + "|" +
                                    g_advancedSettings.m_videoExtensions      + "|" +
                                    g_advancedSettings.GetMusicExtensions()   + "|" +
                                    g_advancedSettings.m_discStubExtensions;
            CDirectory::GetDirectory((const char*)parent_id, items, supported);
            DefaultSortItems(items);
        }

        if (items.CacheToDiscAlways() ||
            (items.CacheToDiscIfSlow() && (XbmcThreads::SystemClockMillis() - time) > 1000 ))
        {
            NPT_AutoLock lock(m_CacheMutex);
            items.Save();
        }
    }

    // as there's no library://music support, manually add playlists and music
    // video nodes
    if (items.GetPath() == "musicdb://")
    {
        CFileItemPtr playlists(new CFileItem("special://musicplaylists/", true));
        playlists->SetLabel(g_localizeStrings.Get(136));
        items.Add(playlists);

        CVideoDatabase database;
        database.Open();

        if (database.HasContent(VIDEODB_CONTENT_MUSICVIDEOS))
        {
            CFileItemPtr mvideos(new CFileItem("library://video/musicvideos/", true));
            mvideos->SetLabel(g_localizeStrings.Get(20389));
            items.Add(mvideos);
        }
    }

    // Don't pass parent_id if action is Search not BrowseDirectChildren, as
    // we want the engine to determine the best parent id, not necessarily the one
    // passed
    NPT_String action_name = action->GetActionDesc().GetName();

    return BuildResponse(action,
                         items,
                         filter,
                         starting_index,
                         requested_count,
                         sort_criteria,
                         context,
                         (action_name.Compare("Search", true) == 0) ? NULL : parent_id.GetChars());
}

NPT_Result CUPnPServer::BuildResponse(PLT_ActionReference&          action,
                                      CFileItemList&                items,
                                      const char*                   filter,
                                      NPT_UInt32                    starting_index,
                                      NPT_UInt32                    requested_count,
                                      const char*                   sort_criteria,
                                      const PLT_HttpRequestContext& context,
                                      const char*                   parent_id)
{
    NPT_COMPILER_UNUSED(sort_criteria);

    CLog::Log(LOGDEBUG,
              "Building UPnP response with filter '%s', starting @ %d with %d requested",
              (const char*)filter,
              starting_index,
              requested_count);

    // we will reuse this ThumbLoader for all items
    NPT_Reference<CThumbLoader> thumb_loader;

    if (URIUtils::IsVideoDb(items.GetPath()) ||
        StringUtils::StartsWithNoCase(items.GetPath(), "library://video/") ||
        StringUtils::StartsWithNoCase(items.GetPath(), "special://profile/playlists/video/"))
    {

        thumb_loader = NPT_Reference<CThumbLoader>(new CVideoThumbLoader());
    }
    else if (URIUtils::IsMusicDb(items.GetPath()) ||
             StringUtils::StartsWithNoCase(items.GetPath(),
                                           "special://profile/playlists/music/"))
    {
        thumb_loader = NPT_Reference<CThumbLoader>(new CMusicThumbLoader());
    }

    if (!thumb_loader.IsNull())
    {
        thumb_loader->OnLoaderStart();
    }

    // this isn't pretty but needed to properly hide the addons node from clients
    if (StringUtils::StartsWith(items.GetPath(), "library"))
    {
        for (int i = 0 ; i < items.Size() ; i++)
        {
            if (StringUtils::StartsWith(items[i]->GetPath(), "addons") ||
                StringUtils::EndsWith(items[i]->GetPath(), "/addons.xml/"))
                items.Remove(i);
        }
    }

    // won't return more than UPNP_MAX_RETURNED_ITEMS items at a time to keep things smooth
    // 0 requested means as many as possible
    NPT_UInt32 max_count  = (requested_count == 0)
                            ? m_MaxReturnedItems:std::min((unsigned long)requested_count,
                                                          (unsigned long)m_MaxReturnedItems);
    NPT_UInt32 stop_index = std::min((unsigned long)(starting_index + max_count), (unsigned long)items.Size()); // don't return more than we can

    NPT_Cardinal count    = 0;
    NPT_Cardinal total    = items.Size();
    NPT_String didl       = didl_header;
    PLT_MediaObjectReference object;

    for (unsigned long i = starting_index ; i < stop_index ; ++i)
    {
        object = Build(items[i], true, context, thumb_loader, parent_id);

        if (object.IsNull())
        {
            // don't tell the client this item ever existed
            --total;
            continue;
        }

        NPT_String tmp;
        NPT_CHECK(PLT_Didl::ToDidl(*object.AsPointer(), filter, tmp));

        // Neptunes string growing is dead slow for small additions
        if (didl.GetCapacity() < tmp.GetLength() + didl.GetLength())
        {
            didl.Reserve((tmp.GetLength() + didl.GetLength())*2);
        }

        didl += tmp;
        ++count;
    }

    didl += didl_footer;

    CLog::Log(LOGDEBUG,
              "Returning UPnP response with %d items out of %d total matches",
              count,
              total);

    NPT_CHECK(action->SetArgumentValue("Result",         didl));
    NPT_CHECK(action->SetArgumentValue("NumberReturned", NPT_String::FromInteger(count)));
    NPT_CHECK(action->SetArgumentValue("TotalMatches",   NPT_String::FromInteger(total)));
    NPT_CHECK(action->SetArgumentValue("UpdateId",       "0"));

    return NPT_SUCCESS;
}

static NPT_String FindSubCriteria(NPT_String criteria, const char* name)
{
    NPT_String result;
    int search = criteria.Find(name);

    if (search >= 0)
    {
        criteria = criteria.Right(criteria.GetLength() - search - NPT_StringLength(name));
        criteria.TrimLeft(" ");

        if (criteria.GetLength() > 0 && criteria[0] == '=')
        {
            criteria.TrimLeft("= ");

            if (criteria.GetLength() > 0 && criteria[0] == '\"')
            {
                search = criteria.Find("\"", 1);

                if (search > 0)
                    result = criteria.SubString(1, search-1);
            }
        }
    }

    return result;
}

NPT_Result CUPnPServer::OnSearchContainer(PLT_ActionReference&          action,
                                          const char*                   object_id,
                                          const char*                   search_criteria,
                                          const char*                   filter,
                                          NPT_UInt32                    starting_index,
                                          NPT_UInt32                    requested_count,
                                          const char*                   sort_criteria,
                                          const PLT_HttpRequestContext& context)
{
    CLog::Log(LOGDEBUG,
              "Received Search request for object '%s' with search '%s'",
              (const char*)object_id,
              (const char*)search_criteria);

    NPT_String id = object_id;

    if (id.StartsWith("musicdb://"))
    {
        // we browse for all tracks given a genre, artist or album
        if (NPT_String(search_criteria).Find("object.item.audioItem") >= 0)
        {
            if (!id.EndsWith("/"))
                id += "/";

            NPT_Cardinal count = id.SubString(10).Split("/").GetItemCount();
            // remove extra empty node count
            count              = count ? count-1 : 0;

            // genre
            if (id.StartsWith("musicdb://genres/"))
            {
                // all tracks of all genres
                if (count == 1)
                    id += "-1/-1/-1/";
                // all tracks of a specific genre
                else if (count == 2)
                    id += "-1/-1/";
                // all tracks of a specific genre of a specific artist
                else if (count == 3)
                    id += "-1/";
            }
            else if (id.StartsWith("musicdb://artists/"))
            {
                // all tracks by all artists
                if (count == 1)
                    id += "-1/-1/";
                // all tracks of a specific artist
                else if (count == 2)
                    id += "-1/";
            }
            else if (id.StartsWith("musicdb://albums/"))
            {
                // all albums ?
                if (count == 1) id += "-1/";
            }
        }

        return OnBrowseDirectChildren(action, id, filter, starting_index, requested_count, sort_criteria, context);
    }
    else if (NPT_String(search_criteria).Find("object.item.audioItem") >= 0)
    {
        // look for artist, album & genre filters
        NPT_String genre  = FindSubCriteria(search_criteria, "upnp:genre");
        NPT_String album  = FindSubCriteria(search_criteria, "upnp:album");
        NPT_String artist = FindSubCriteria(search_criteria, "upnp:artist");
        // sonos looks for microsoft specific stuff
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:artistPerformer");
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:artistAlbumArtist");
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:authorComposer");

        CMusicDatabase database;
        database.Open();

        if (genre.GetLength() > 0)
        {
            // all tracks by genre filtered by artist and/or album
            std::string strPath = StringUtils::Format("musicdb://genres/%i/%i/%i/",
                                          database.GetGenreByName((const char*)genre),
                                          database.GetArtistByName((const char*)artist), // will return -1 if no artist
                                          database.GetAlbumByName((const char*)album));  // will return -1 if no album

            return OnBrowseDirectChildren(action, strPath.c_str(), filter, starting_index, requested_count, sort_criteria, context);
        }
        else if (artist.GetLength() > 0)
        {
            // all tracks by artist name filtered by album if passed
            std::string strPath = StringUtils::Format("musicdb://artists/%i/%i/",
                                          database.GetArtistByName((const char*)artist),
                                          database.GetAlbumByName((const char*)album)); // will return -1 if no album

            return OnBrowseDirectChildren(action, strPath.c_str(), filter, starting_index, requested_count, sort_criteria, context);
        }
        else if (album.GetLength() > 0)
        {
            // all tracks by album name
            std::string strPath = StringUtils::Format("musicdb://albums/%i/",
                                                      database.GetAlbumByName((const char*)album));

            return OnBrowseDirectChildren(action, strPath.c_str(), filter, starting_index, requested_count, sort_criteria, context);
        }

        // browse all songs
        return OnBrowseDirectChildren(action, "musicdb://songs/", filter, starting_index, requested_count, sort_criteria, context);
    }
    else if (NPT_String(search_criteria).Find("object.container.album.musicAlbum") >= 0)
    {
        // sonos filters by genre
        NPT_String genre  = FindSubCriteria(search_criteria, "upnp:genre");

        // 360 hack: artist/albums using search
        NPT_String artist = FindSubCriteria(search_criteria, "upnp:artist");
        // sonos looks for microsoft specific stuff
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:artistPerformer");
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:artistAlbumArtist");
        artist            = artist.GetLength()?artist:FindSubCriteria(search_criteria, "microsoft:authorComposer");

        CMusicDatabase database;
        database.Open();

        if (genre.GetLength() > 0)
        {
            std::string strPath = StringUtils::Format("musicdb://genres/%i/%i/",
                                                      database.GetGenreByName((const char*)genre),
                                                      database.GetArtistByName((const char*)artist)); // no artist should return -1
            return OnBrowseDirectChildren(action,
                                          strPath.c_str(),
                                          filter,
                                          starting_index,
                                          requested_count,
                                          sort_criteria,
                                          context);
        }
        else if (artist.GetLength() > 0)
        {
            std::string strPath = StringUtils::Format("musicdb://artists/%i/",
                                                      database.GetArtistByName((const char*)artist));

            return OnBrowseDirectChildren(action,
                                          strPath.c_str(),
                                          filter,
                                          starting_index,
                                          requested_count,
                                          sort_criteria, context);
        }

        // all albums
        return OnBrowseDirectChildren(action,
                                      "musicdb://albums/",
                                      filter,
                                      starting_index,
                                      requested_count,
                                      sort_criteria,
                                      context);
    }
    else if (NPT_String(search_criteria).Find("object.container.person.musicArtist") >= 0)
    {
        // Sonos filters by genre
        NPT_String genre = FindSubCriteria(search_criteria, "upnp:genre");

        if (genre.GetLength() > 0)
        {
            CMusicDatabase database;
            database.Open();
            std::string strPath = StringUtils::Format("musicdb://genres/%i/", database.GetGenreByName((const char*)genre));

            return OnBrowseDirectChildren(action,
                                          strPath.c_str(),
                                          filter,
                                          starting_index,
                                          requested_count,
                                          sort_criteria,
                                          context);
        }

        return OnBrowseDirectChildren(action,
                                      "musicdb://artists/",
                                      filter,
                                      starting_index,
                                      requested_count,
                                      sort_criteria,
                                      context);
    }
    else if (NPT_String(search_criteria).Find("object.container.genre.musicGenre") >= 0)
    {
        return OnBrowseDirectChildren(action,
                                      "musicdb://genres/",
                                      filter,
                                      starting_index,
                                      requested_count,
                                      sort_criteria,
                                      context);
    }
    else if (NPT_String(search_criteria).Find("object.container.playlistContainer") >= 0)
    {
        return OnBrowseDirectChildren(action,
                                      "special://musicplaylists/",
                                      filter,
                                      starting_index,
                                      requested_count,
                                      sort_criteria,
                                      context);
    }
    else if (NPT_String(search_criteria).Find("object.item.videoItem") >= 0)
    {
        CFileItemList  items, itemsall;
        CVideoDatabase database;

        if (!database.Open())
        {
            action->SetError(800, "Internal Error");
            return NPT_SUCCESS;
        }

        if (!database.GetMoviesNav("videodb://movies/titles/", items))
        {
            action->SetError(800, "Internal Error");
            return NPT_SUCCESS;
        }

        itemsall.Append(items);
        items.Clear();

        if (!database.GetEpisodesByWhere("videodb://tvshows/titles/",
                                         CDatabase::Filter(),
                                         items))
        {
            action->SetError(800, "Internal Error");
            return NPT_SUCCESS;
        }

        itemsall.Append(items);
        items.Clear();

        if (!database.GetMusicVideosByWhere("videodb://musicvideos/titles/",
                                            CDatabase::Filter(),
                                            items))
        {
            action->SetError(800, "Internal Error");
            return NPT_SUCCESS;
        }

        itemsall.Append(items);
        items.Clear();

        return BuildResponse(action,
                             itemsall,
                             filter,
                             starting_index,
                             requested_count,
                             sort_criteria,
                             context,
                             NULL);
    }
    else if (NPT_String(search_criteria).Find("object.item.imageItem") >= 0)
    {
        CFileItemList items;

        return BuildResponse(action,
                             items,
                             filter,
                             starting_index,
                             requested_count,
                             sort_criteria,
                             context,
                             NULL);
    }

    return NPT_FAILURE;
}

NPT_Result CUPnPServer::OnUpdateObject(PLT_ActionReference&             action,
                                       const char*                      object_id,
                                       NPT_Map<NPT_String,NPT_String>&  current_vals,
                                       NPT_Map<NPT_String,NPT_String>&  new_vals,
                                       const PLT_HttpRequestContext&    context)
{
    std::string path(CURL::Decode(object_id));
    CFileItem updated;
    updated.SetPath(path);

    CLog::Log(LOGINFO,
              "UPnP: OnUpdateObject: %s from %s",
              path.c_str(),
              (const char*)context.GetRemoteAddress().GetIpAddress().ToString());

    NPT_String playCount, position;
    int err;
    const char* msg = NULL;
    bool updatelisting(false);

    // we pause eventing as multiple announces may happen in this operation
    PLT_Service* service = NULL;
    NPT_CHECK_LABEL(FindServiceById("urn:upnp-org:serviceId:ContentDirectory", service), error);
    NPT_CHECK_LABEL(service->PauseEventing(), error);

    if (updated.IsVideoDb())
    {
        CVideoDatabase db;
        NPT_CHECK_LABEL(!db.Open(), error);

        // must first determine type of file from object id
        VIDEODATABASEDIRECTORY::CQueryParams params;
        VIDEODATABASEDIRECTORY::CDirectoryNode::GetDatabaseInfo(path.c_str(), params);

        int id = -1;
        VIDEODB_CONTENT_TYPE content_type;

        if      ((id = params.GetMovieId()) >= 0)
            content_type = VIDEODB_CONTENT_MOVIES;
        else if ((id = params.GetEpisodeId()) >= 0)
            content_type = VIDEODB_CONTENT_EPISODES;
        else if ((id = params.GetMVideoId()) >= 0)
            content_type = VIDEODB_CONTENT_MUSICVIDEOS;
        else
        {
            err = 701;
            msg = "No such object";
            goto failure;
        }

        std::string file_path;
        db.GetFilePathById(id, file_path, content_type);
        CVideoInfoTag tag;
        db.LoadVideoInfo(file_path, tag);
        updated.SetFromVideoInfoTag(tag);
        CLog::Log(LOGINFO, "UPNP: Translated to %s", file_path.c_str());

        position  = new_vals["lastPlaybackPosition"];
        playCount = new_vals["playCount"];

        if (!position.IsEmpty() && position.Compare(current_vals["lastPlaybackPosition"]) != 0)
        {
            NPT_UInt32 resume;
            NPT_CHECK_LABEL(position.ToInteger32(resume), args);

            if (resume <= 0)
                db.ClearBookMarksOfFile(file_path, CBookmark::RESUME);
            else
            {
                CBookmark bookmark;
                bookmark.timeInSeconds      = resume;
                bookmark.totalTimeInSeconds = resume + 100; // not required to be correct
                bookmark.playerState        = new_vals["lastPlayerState"];

                db.AddBookMarkToFile(file_path, bookmark, CBookmark::RESUME);
            }

            if (playCount.IsEmpty())
            {
                CVariant data;
                data["id"]   = updated.GetVideoInfoTag()->m_iDbId;
                data["type"] = updated.GetVideoInfoTag()->m_type;
                ANNOUNCEMENT::CAnnouncementManager::GetInstance().Announce(ANNOUNCEMENT::VideoLibrary, "xbmc", "OnUpdate", data);
            }

            updatelisting = true;
        }

        if (!playCount.IsEmpty() && playCount.Compare(current_vals["playCount"]) != 0)
        {
            NPT_UInt32 count;
            NPT_CHECK_LABEL(playCount.ToInteger32(count), args);
            db.SetPlayCount(updated, count);
            updatelisting = true;
        }

        // we must load the changed settings before propagating to local UI
        if (updatelisting)
        {
            db.LoadVideoInfo(file_path, tag);
            updated.SetFromVideoInfoTag(tag);
        }

    }
    else if (updated.IsMusicDb())
    {
      //! @todo implement this
    }
    else
    {
        err = 701;
        msg = "No such object";
        goto failure;
    }

    if (updatelisting)
    {
        updated.SetPath(path);

        if      (updated.IsVideoDb())
             CUtil::DeleteVideoDatabaseDirectoryCache();
        else if (updated.IsMusicDb())
             CUtil::DeleteMusicDatabaseDirectoryCache();

        CFileItemPtr msgItem(new CFileItem(updated));
        CGUIMessage message(GUI_MSG_NOTIFY_ALL,
                            g_windowManager.GetActiveWindow(),
                            0,
                            GUI_MSG_UPDATE_ITEM,
                            1,
                            msgItem);
        g_windowManager.SendThreadMessage(message);
    }

    NPT_CHECK_LABEL(service->PauseEventing(false), error);
    return NPT_SUCCESS;

args:

    err = 402;
    msg = "Invalid args";
    goto failure;

error:

    err = 501;
    msg = "Internal error";

failure:

    CLog::Log(LOGERROR, "UPNP: OnUpdateObject failed with err %d:%s", err, msg);
    action->SetError(err, msg);
    service->PauseEventing(false);
    return NPT_FAILURE;
}

NPT_Result CUPnPServer::ServeFile(const NPT_HttpRequest&        request,
                                  const NPT_HttpRequestContext& context,
                                  NPT_HttpResponse&             response,
                                  const NPT_String&             md5)
{
    // Translate hash to filename
    NPT_String file_path(md5), *file_path2;
    {
        NPT_AutoLock lock(m_FileMutex);

        if (NPT_SUCCEEDED(m_FileMap.Get(md5, file_path2)))
        {
            file_path = *file_path2;
            CLog::Log(LOGDEBUG, "Received request to serve '%s' = '%s'", (const char*)md5, (const char*)file_path);
        }
        else
        {
            CLog::Log(LOGDEBUG, "Received request to serve unknown md5 '%s'", (const char*)md5);
            response.SetStatus(404, "File Not Found");
            return NPT_SUCCESS;
        }
    }

    // File requested
    NPT_HttpUrl rooturi(context.GetLocalAddress().GetIpAddress().ToString(), context.GetLocalAddress().GetPort(), "/");

    if (file_path.Left(8).Compare("stack://", true) == 0)
    {
        NPT_List<NPT_String> files = file_path.SubString(8).Split(" , ");

        if (files.GetItemCount() == 0)
        {
            response.SetStatus(404, "File Not Found");
            return NPT_SUCCESS;
        }

        NPT_String output;
        output.Reserve(file_path.GetLength()*2);
        output += "#EXTM3U\r\n";

        NPT_List<NPT_String>::Iterator url = files.GetFirstItem();

        for (; url ; url++)
        {
            output += ("#EXTINF:-1," + URIUtils::GetFileName((const char*)*url)).c_str();
            output += "\r\n";
            output += BuildSafeResourceUri(
                          rooturi,
                          context.GetLocalAddress().GetIpAddress().ToString(),
                          *url);
            output += "\r\n";
        }

        PLT_HttpHelper::SetBody(response, (const char*)output, output.GetLength());
        response.GetHeaders().SetHeader("Content-Disposition", "inline; filename=\"stack.m3u\"");
        return NPT_SUCCESS;
    }

    if (URIUtils::IsURL((const char*)file_path))
    {
      std::string disp = "inline; filename=\"" + URIUtils::GetFileName((const char*)file_path) + "\"";
      response.GetHeaders().SetHeader("Content-Disposition", disp.c_str());
    }

    // set getCaptionInfo.sec - sets subtitle uri for Samsung devices
    const NPT_String* captionInfoHeader = request.GetHeaders().GetHeaderValue("getCaptionInfo.sec");

    if (captionInfoHeader) 
    {
        NPT_String* sub_uri, movie;
        movie = "subtitle://" + md5;

        NPT_AutoLock lock(m_FileMutex);

        if (NPT_SUCCEEDED(m_FileMap.Get(movie, sub_uri))) 
        {
            response.GetHeaders().SetHeader("CaptionInfo.sec", sub_uri->GetChars(), false);
        }
    }

    return PLT_HttpServer::ServeFile(request,
                                     context,
                                     response,
                                     file_path);
}

NPT_Result CUPnPServer::AddSubtitleUriForSecResponse(NPT_String movie_md5,
                                                     NPT_String subtitle_uri)
{
    /* using existing m_FileMap to store subtitle uri for movie,
       adding subtitle:// prefix, because there is already entry for movie md5 with movie path */
    NPT_String movie = "subtitle://" + movie_md5;

    NPT_AutoLock lock(m_FileMutex);
    NPT_CHECK(m_FileMap.Put(movie, subtitle_uri));

    return NPT_SUCCESS;
}

void CUPnPServer::AddSafeResourceUri(PLT_MediaObject*        object,
                                     const NPT_HttpUrl&      rooturi,
                                     NPT_List<NPT_IpAddress> ips,
                                     const char*             file_path,
                                     const NPT_String&       info)
{
    PLT_MediaItemResource res;

    for (NPT_List<NPT_IpAddress>::Iterator ip = ips.GetFirstItem() ; ip ; ++ip)
    {
        res.m_ProtocolInfo = PLT_ProtocolInfo(info);
        res.m_Uri          = BuildSafeResourceUri(rooturi, (*ip).ToString(), file_path);
        object->m_Resources.Add(res);
    }
}

NPT_String CUPnPServer::GetParentFolder(NPT_String file_path)
{
    int index = file_path.ReverseFind("\\");

    if (index == -1)
        return "";

    return file_path.Left(index);
}

/*----------------------------------------------------------------------
|   CUPnPServer::SortItems
|
|   Only support upnp: & dc: namespaces for now.
|   Other servers add their own vendor-specific sort methods. This could
|   possibly be handled with 'quirks' in the long run.
|
|   return true if sort criteria was matched
+---------------------------------------------------------------------*/
/*
bool CUPnPServer::SortItems(CFileItemList& items, const char* sort_criteria)
{
    std::string criteria(sort_criteria);

    if (criteria.empty()
    {
        return false;
    }

    bool sorted                     = false;
    std::vector<std::string> tokens = StringUtils::Split(criteria, ",");

    for (std::vector<std::string>::reverse_iterator itr = tokens.rbegin() ;
         itr != tokens.rend() ; ++itr)
    {
        SortDescription sorting;
        // Platinum guarantees 1st char is - or + 
        sorting.sortOrder  = StringUtils::StartsWith(*itr, "+") ? SortOrderAscending : SortOrderDescending;
        std::string method = itr->substr(1);

        // resource specific
        if (StringUtils::EqualsNoCase(method, "res@duration"))
        sorting.sortBy = SortByTime;
        else if (StringUtils::EqualsNoCase(method, "res@size"))
        sorting.sortBy = SortBySize;
        else if (StringUtils::EqualsNoCase(method, "res@bitrate"))
        sorting.sortBy = SortByBitrate;

        // dc:
        else if (StringUtils::EqualsNoCase(method, "dc:date"))
            sorting.sortBy = SortByDate;
        else if (StringUtils::EqualsNoCase(method, "dc:title"))
        {
            sorting.sortBy = SortByTitle;
            sorting.sortAttributes = SortAttributeIgnoreArticle;
        }

        // upnp:
        else if (StringUtils::EqualsNoCase(method, "upnp:album"))
            sorting.sortBy = SortByAlbum;
        else if (StringUtils::EqualsNoCase(method, "upnp:artist") ||
                 StringUtils::EqualsNoCase(method, "upnp:albumArtist"))
            sorting.sortBy = SortByArtist;
        else if (StringUtils::EqualsNoCase(method, "upnp:episodeNumber"))
            sorting.sortBy = SortByEpisodeNumber;
        else if (StringUtils::EqualsNoCase(method, "upnp:episodeCount"))
            sorting.sortBy = SortByNumberOfEpisodes;
        else if (StringUtils::EqualsNoCase(method, "upnp:episodeSeason"))
            sorting.sortBy = SortBySeason;
        else if (StringUtils::EqualsNoCase(method, "upnp:genre"))
            sorting.sortBy = SortByGenre;
        else if (StringUtils::EqualsNoCase(method, "upnp:originalTrackNumber"))
            sorting.sortBy = SortByTrackNumber;
        else if(StringUtils::EqualsNoCase(method, "upnp:rating"))
            sorting.sortBy = SortByMPAA;
        else if (StringUtils::EqualsNoCase(method, "xbmc:rating"))
            sorting.sortBy = SortByRating;
        else if (StringUtils::EqualsNoCase(method, "xbmc:dateadded"))
            sorting.sortBy = SortByDateAdded;
        else if (StringUtils::EqualsNoCase(method, "xbmc:votes"))
            sorting.sortBy = SortByVotes;
        else
        {
            CLog::Log(LOGINFO, "UPnP: unsupported sort criteria '%s' passed", method.c_str());
            continue; // needed so unidentified sort methods don't re-sort by label
        }

        CLog::Log(LOGINFO, "UPnP: Sorting by method %d, order %d, attributes %d", sorting.sortBy, sorting.sortOrder, sorting.sortAttributes);
        items.Sort(sorting);
        sorted = true;
    }

    return sorted;
}
*/

/*
void CUPnPServer::DefaultSortItems(CFileItemList& items)
{
    CGUIViewState* const viewState = CGUIViewState::GetViewState(items.IsVideoDb()
                                     ? WINDOW_VIDEO_NAV : -1, items);
    if (viewState)
    {
        SortDescription sorting = viewState->GetSortMethod();
        items.Sort(sorting.sortBy, sorting.sortOrder, sorting.sortAttributes);
        delete viewState;
    }
}
*/

} // namespace UPNP
