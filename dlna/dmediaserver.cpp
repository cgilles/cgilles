/* ============================================================
 *
 * This file is a part of digiKam project
 * http://www.digikam.org
 *
 * Date        : 2017-09-24
 * Description : a media server to export collections through DLNA.
 *
 * Copyright (C) 2017 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "dmediaserver.h"

// Qt includes

#include <QDebug>
#include <QList>
#include <QUrl>
#include <QFile>
#include <QStandardPaths>

// Platinum includes

#include "Platinum.h"

// Local includes

#include "dlnaserver.h"

void NPT_Console::Output(const char* msg)
{
    qDebug() << msg;
}

int ConvertLogLevel(int nptLogLevel)
{
    return 0;
/*
    if (nptLogLevel >= NPT_LOG_LEVEL_FATAL)
        return LOGFATAL;
    if (nptLogLevel >= NPT_LOG_LEVEL_SEVERE)
        return LOGERROR;
    if (nptLogLevel >= NPT_LOG_LEVEL_WARNING)
        return LOGWARNING;
    if (nptLogLevel >= NPT_LOG_LEVEL_INFO)
        return LOGNOTICE;
    if (nptLogLevel >= NPT_LOG_LEVEL_FINE)
        return LOGINFO;

    return LOGDEBUG;*/
}

void UPnPLogger(const NPT_LogRecord* record)
{
    //CLog::Log(ConvertLogLevel(record->m_Level), LOGUPNP, "Platinum [%s]: %s", record->m_LoggerName, record->m_Message);
}

namespace Digikam
{

class CDeviceHostReferenceHolder
{
public:

    PLT_DeviceHostReference m_device;
};

class DMediaServer::Private
{
public:

    Private()
      : logHandler(NULL),
        serverHolder(new CDeviceHostReferenceHolder())
    {
        NPT_LogManager::GetDefault().Configure("plist:.level=FINE;.handlers=CustomHandler;");
        NPT_LogHandler::Create("digiKam", "CustomHandler", logHandler);
        logHandler->SetCustomHandlerFunction(&UPnPLogger);
    }

    NPT_LogHandler*             logHandler;
    CDeviceHostReferenceHolder* serverHolder;
};

DMediaServer::DMediaServer(QObject* const parent)
    : QObject(parent),
      d(new Private)
{
}

bool DMediaServer::init(int port)
{
    DLNAMediaServer* const device = new DLNAMediaServer(
                                    "digiKam Media Server",
                                    false,
                                    NULL,
                                    port);

    device->m_ModelName        = "digiKam";
    device->m_ModelNumber      = "5.0.0";
    device->m_ModelDescription = "digiKam - Media Server";
    device->m_ModelURL         = "https://www.digiKam.org/";
    device->m_Manufacturer     = "digiKam.org";
    device->m_ManufacturerURL  = "https://www.digiKam.org/";
    device->SetDelegate(device);

    d->serverHolder->m_device  = device;

    return true;
}

PLT_DeviceHostReference& DMediaServer::server()
{
    return d->serverHolder->m_device;
}

DMediaServer::~DMediaServer()
{
    delete d->logHandler;
    delete d->serverHolder;
    delete d;
}

void DMediaServer::addAlbumsOnServer(const MediaServerMap& map)
{
    static_cast<DLNAMediaServer*>(server().AsPointer())->addAlbumsOnServer(map);
}

} // namespace Digikam
