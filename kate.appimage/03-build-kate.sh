#!/bin/bash

# Script to build Kate using CentOS 6.
# This script must be run as sudo
#
# Copyright (c) 2015-2017, Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#

# Halt on errors
set -e

#################################################################################################
# Manage script traces to log file

mkdir -p ./logs
exec > >(tee ./logs/build-kate.full.log) 2>&1

#################################################################################################

echo "02-build-extralibs.sh : build extra libraries under CentoOS 6."
echo "--------------------------------------------------------------"

#################################################################################################
# Pre-processing checks

. ./config.sh
. ./common.sh
StartScript
ChecksCPUCores
CentOS6Adjustments
. /opt/rh/devtoolset-3/enable

ORIG_WD="`pwd`"

#################################################################################################

echo -e "---------- Build Applications\n"

cd $BUILDING_DIR

rm -rf $BUILDING_DIR/* || true

cmake3 $ORIG_WD/3rdparty \
       -DCMAKE_INSTALL_PREFIX:PATH=/usr \
       -DINSTALL_ROOT=/usr \
       -DEXTERNALS_DOWNLOAD_DIR=$DOWNLOAD_DIR

cmake3 --build . --config RelWithDebInfo --target ext_kdesvn      -- -j$CPU_CORES
cmake3 --build . --config RelWithDebInfo --target ext_konsole     -- -j$CPU_CORES
cmake3 --build . --config RelWithDebInfo --target ext_kate        -- -j$CPU_CORES
cmake3 --build . --config RelWithDebInfo --target ext_valkyrie    -- -j$CPU_CORES
cmake3 --build . --config RelWithDebInfo --target ext_kompare     -- -j$CPU_CORES
cmake3 --build . --config RelWithDebInfo --target ext_kcachegrind -- -j$CPU_CORES

#################################################################################################

echo -e "---------- Build Application translations\n"

cd $ORIG_WD/i18n

cmake3 -DCMAKE_INSTALL_PREFIX="/usr" \
       -DCMAKE_BUILD_TYPE=debug \
       -DCMAKE_COLOR_MAKEFILE=ON \
       -Wno-dev \
       .

make -j$CPU_CORES

make install/fast && cd "$ORIG_WD"

TerminateScript
