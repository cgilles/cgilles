#
# Copyright (c) 2015-2017 by Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (POLICY CMP0063)
    cmake_policy(SET CMP0063 NEW)
endif (POLICY CMP0063)

cmake_minimum_required(VERSION 3.0.0)

find_package(ECM 5.20 CONFIG REQUIRED)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH})

include(ECMGenerateHeaders)
include(ECMGeneratePriFile)
include(ECMSetupVersion)
include(ECMOptionalAddSubdirectory)
include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDEFrameworkCompilerSettings)
include(CMakePackageConfigHelpers)
include(FeatureSummary)

find_package(Gettext REQUIRED)

if(NOT EXISTS ${CMAKE_SOURCE_DIR}/po/)

    find_package(Ruby)
    find_package(Subversion)

    if(RUBY_EXECUTABLE AND Subversion_FOUND)

        message(STATUS "Extract application translation files from git repositories. Please wait, it can take a while...")

        execute_process(COMMAND ${RUBY_EXECUTABLE} "${CMAKE_SOURCE_DIR}/fetch_l10n_po.rb"
                    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")

        message(STATUS "Application translation files extraction done.")

    endif()

endif()

set_property(GLOBAL PROPERTY ALLOW_DUPLICATE_CUSTOM_TARGETS 1)

add_subdirectory(po)
