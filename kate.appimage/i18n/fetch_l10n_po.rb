#!/usr/bin/env ruby
#
# Ruby script for pulling l10n application translations
# Requires ruby version >= 1.9
#
# Copyright (c) 2015-2017, Gilles Caulier, <caulier dot gilles at gmail dot com>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

require 'rbconfig'
STDOUT.sync = true

branch = "trunk"
i18nlangs = []
i18nlangs = `cat ./subdirs`

##########################################################################################
# EXTRACT TRANSLATED APPLICATION FILES

if !(File.exists?("po") && File.directory?("po"))
    Dir.mkdir( "po" )
end

Dir.chdir( "po" )
topmakefile = File.new( "CMakeLists.txt", File::CREAT | File::RDWR | File::TRUNC )

i18nlangs.each_line do |lang|

    lang.chomp!()

    if (lang != nil && lang != "")

        print ("#{lang} ")

        if !(File.exists?(lang) && File.directory?(lang))
            Dir.mkdir(lang)
        end

        Dir.chdir(lang)

        makefile = File.new( "CMakeLists.txt", File::CREAT | File::RDWR | File::TRUNC )
        makefile << "file(GLOB _po_files *.po)\n"
        makefile << "GETTEXT_PROCESS_PO_FILES( #{lang} ALL INSTALL_DESTINATION ${LOCALE_INSTALL_DIR} PO_FILES ${_po_files} )\n"
        makefile.close()

        # Kate

        for part in ['kate',
                     'kate-ctags-plugin',
                     'katebacktracebrowserplugin',
                     'katebuild-plugin',
                     'katecloseexceptplugin',
                     'katefilebrowserplugin',
                     'katefiletree',
                     'kategdbplugin',
                     'katekonsoleplugin',
                     'kateopenheader',
                     'kateproject',
                     'katesearch',
                     'katesnippetsplugin',
                     'katesql',
                     'katesymbolviewer',
                     'katetextfilter',
                     'katexmltools',
                     'kwrite',
                     'konsole'
                    ]



            `svn cat svn://anonsvn.kde.org/home/kde/#{branch}/l10n-kf5/#{lang}/messages/applications/#{part}.po 2> /dev/null | tee #{part}.po `

            if FileTest.size( "#{part}.po" ) == 0
                File.delete( "#{part}.po" )
            end
        end

        Dir.chdir("..")
        topmakefile << "add_subdirectory( #{lang} )\n"

    end
end

topmakefile.close()
puts ("\n")

