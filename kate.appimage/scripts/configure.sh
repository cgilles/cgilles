#!/bin/sh

. /opt/rh/devtoolset-3/enable

if [ ! -d Build/Release ]; then
 mkdir -p Build/Release
fi

if [ ! -d Build/Release ]; then
  echo "Could not change to build folder"
  exit -1
fi

cd Build/Release||exit

cmake3 ../.. -DCMAKE_BUILD_TYPE=RelWithDebInfo 
cd -

