#!/bin/sh

# Manage build sub-dir
rm -rf build
mkdir build
cd build

cmake . \
      -DCMAKE_BUILD_TYPE=Debug \
      -G"Unix Makefiles" \
      -Wno-dev \
      ..
