/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef HDRCREATOR_H
#define HDRCREATOR_H

// Qt includes

#include <QObject>

// KDE includes

#include <kurl.h>

namespace KIPI
{
    class Interface;
}

namespace KIPIHDRCreatorPlugin
{
  
class HdrCreator : public QObject
{
    Q_OBJECT

public:

    HdrCreator(const KUrl::List& urls, KIPI::Interface* iface, QObject* parent);
    ~HdrCreator();

private Q_SLOTS:

    void slotHrdWizardFinished(void* data);

private:

    KIPI::Interface* m_iface;
};

} // namespace KIPIHDRCreatorPlugin

#endif /* HDRCREATOR_H */
