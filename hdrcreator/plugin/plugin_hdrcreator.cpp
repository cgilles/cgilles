/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "plugin_hdrcreator.moc"

// KDE includes

#include <kaction.h>
#include <kactioncollection.h>
#include <kapplication.h>
#include <kconfig.h>
#include <kdebug.h>
#include <kgenericfactory.h>
#include <kiconloader.h>
#include <klibloader.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kwindowsystem.h>

// LibKIPI includes

#include <libkipi/imagecollection.h>
#include <libkipi/interface.h>

// Local includes

#include "hdrcreator.h"

namespace KIPIHDRCreatorPlugin
{

K_PLUGIN_FACTORY( HDRCreatorFactory, registerPlugin<Plugin_HDRCreator>(); )
K_EXPORT_PLUGIN ( HDRCreatorFactory("kipiplugin_hdrcreator") )

Plugin_HDRCreator::Plugin_HDRCreator(QObject* const parent, const QVariantList&)
    : Plugin(HDRCreatorFactory::componentData(), parent, "HDRCreator")
{
    m_action       = 0;
    m_parentWidget = 0;
    m_interface    = 0;

    setUiBaseName("kipiplugin_hdrcreatorui.rc");
    setupXML();

    kDebug(AREA_CODE_LOADING) << "Plugin_HDRCreator plugin loaded";
}

Plugin_HDRCreator::~Plugin_HDRCreator()
{
}

void Plugin_HDRCreator::setup(QWidget* const widget)
{
    m_parentWidget = widget;
    Plugin::setup(m_parentWidget);

    setupActions();

    m_interface = interface();

    if (!m_interface)
    {
       kError() << "Kipi interface is null!";
       return;
    }

    m_action->setEnabled(true);
}

void Plugin_HDRCreator::setupActions()
{
    setDefaultCategory(ToolsPlugin);

    m_action = new KAction(this);
    m_action->setText(i18n("Create HDR images..."));
    m_action->setIcon(KIcon("hdrcreator"));
    m_action->setEnabled(false);

    connect(m_action, SIGNAL(triggered(bool)),
            this, SLOT(slotActivate()));

    addAction("hdrcreator", m_action);
}

void Plugin_HDRCreator::slotActivate()
{
    if (!m_interface)
    {
        kError() << "Kipi interface is null!";
        return;
    }

    ImageCollection images = m_interface->currentSelection();

    if (!images.isValid())
        return;

    KUrl::List urls = images.images();
    new HdrCreator(urls, m_interface, this);
}

} // namespace KIPIHDRCreatorPlugin
