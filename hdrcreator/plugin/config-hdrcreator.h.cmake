#ifndef CONFIG_HDRCREATOR_H
#define CONFIG_HDRCREATOR_H

/* Define to 1 if you have OpenEXR shared library installed */
#cmakedefine HAVE_OPENEXR 1

/* Define to 1 if you have OpenMP compiler extensions installed */
#cmakedefine HAVE_OPENMP 1

#endif /* CONFIG_HDRCREATOR_H */
