[Desktop Entry]
Encoding=UTF-8
Name=HDRCreator
Name[x-test]=xxHDRCreatorxx
Comment=A tool to create HDR images
Comment[x-test]=xxA tool to create HDR imagesxx
Icon=hdrcreator
ServiceTypes=KIPI/Plugin
Type=Service
X-KDE-Library=kipiplugin_hdrcreator
X-KIPI-PluginCategories=Tools
X-KIPI-BinaryVersion=${KIPI_SO_VERSION}
author=Gilles Caulier, caulier dot gilles at gmail dot com
