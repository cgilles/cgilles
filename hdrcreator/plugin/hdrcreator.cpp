/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "hdrcreator.moc"

// KDE includes

#include <kapplication.h>
#include <kdebug.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kwindowsystem.h>

// LibKIPI includes

#include <libkipi/interface.h>

// Local includes

#include "newhdrwizard.h"
#include "tonemappingdialog.h"

namespace KIPIHDRCreatorPlugin
{

HdrCreator::HdrCreator(const KUrl::List& urls, KIPI::Interface* iface, QObject* parent)
          : QObject(parent), m_iface(iface)
{
    NewHdrWizard* hdrWizard = new NewHdrWizard(kapp->activeWindow(), urls, iface);

    connect(hdrWizard, SIGNAL(signalFinished(void*)),
            this, SLOT(slotHrdWizardFinished(void*)));

    hdrWizard->show();
}

HdrCreator::~HdrCreator()
{
}

void HdrCreator::slotHrdWizardFinished(void* data)
{
    if (data)
    {
        pfs::Frame* hdr                   = static_cast<pfs::Frame*>(data);
        TonemappingWindow* toneMappingDlg = new TonemappingWindow(kapp->activeWindow(), hdr, QString("hdr"), m_iface);
        toneMappingDlg->setAttribute(Qt::WA_DeleteOnClose);
        toneMappingDlg->show();
    }
}

} // namespace KIPIHDRCreatorPlugin