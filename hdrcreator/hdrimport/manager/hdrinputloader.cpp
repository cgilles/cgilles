/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "hdrinputloader.moc"

// Qt includes

#include <QFileInfo>
#include <QProcess>
#include <QCoreApplication>

// LibKDcraw includes

#include <libkdcraw/kdcraw.h>
#include <libkdcraw/rawdecodingsettings.h>

// Local includes

#include "global.h"
#include "pfstiff.h"
#include "kpwriteimage.h"
#include "kpmetadata.h"
#include "kpversion.h"

using namespace KDcrawIface;
using namespace KIPIPlugins;

namespace KIPIHDRCreatorPlugin
{

hdrInputLoader::hdrInputLoader(const QString& filename, int image_idx)
              : QThread(0), image_idx(image_idx), fname(filename)
{
}

hdrInputLoader::~hdrInputLoader()
{
    wait();
}

void hdrInputLoader::run()
{
    try
    {
        QFileInfo qfi(fname);

        // get exposure time, -1 is error
        float expotime    = obtain_avg_lum( qfi.filePath() );

        QString extension = qfi.suffix().toUpper(); //get filename extension

        // now go and fill the list of image data (real payload)
        // check for extension: if JPEG:
        if (extension.startsWith("JP"))
        {
            QImage *newimage = new QImage(qfi.filePath());
            if (newimage->isNull())
                throw "Failed Loading Image";

            emit ldrReady(newimage, image_idx, expotime, fname, false);
            return;
        }
        else if(extension.startsWith("TIF"))
        {
            TiffReader reader(QFile::encodeName(qfi.filePath()).constData());

            if (reader.is8bitTiff())
            {
                // if 8bit ldr tiff
                QImage *newimage=reader.readIntoQImage();

                emit ldrReady(newimage, image_idx, expotime, fname, true);
                return;
            }
            else if (reader.is16bitTiff())
            {
                // if 16bit (tiff) treat as hdr
                pfs::Frame *frame=reader.readIntoPfsFrame();

                emit mdrReady(frame, image_idx, expotime, fname);
                return;
            }
            else
            {
                // error if other tiff type
                emit loadFailed(tr("ERROR: The file<br>%1<br> is not a 8 bit or 16 bit tiff.").arg(qfi.fileName()), image_idx);
                return;
            }
        }
        else
        {
            // not a jpeg of tiff file, so it's raw input (hdr)

            int                 width, height, rgbmax;
            QByteArray          imageData;
            KDcraw              rawdec;
            RawDecodingSettings settings;
            settings.sixteenBitsImage = true;
            if (!rawdec.decodeRAWImage(fname, settings, imageData, width, height, rgbmax))
            {
                emit loadFailed(tr("ERROR: Cannot decode RAW file: %1").arg(qfi.fileName()), image_idx);
                return;
            }

            uchar* sptr  = (uchar*)imageData.data();
            float factor = 65535.0 / rgbmax;
            unsigned short tmp16[3];

            // Set RGB color components.
            for (int i = 0 ; i < width * height ; i++)
            {
                // Swap Red and Blue and re-ajust color component values
                tmp16[0] = (unsigned short)((sptr[5]*256 + sptr[4]) * factor);      // Blue
                tmp16[1] = (unsigned short)((sptr[3]*256 + sptr[2]) * factor);      // Green
                tmp16[2] = (unsigned short)((sptr[1]*256 + sptr[0]) * factor);      // Red
                memcpy(&sptr[0], &tmp16[0], 6);
                sptr += 6;
            }

            KPMetadata meta;
            meta.load(fname);
            meta.setImageProgramId(QString("Kipi-plugins"), QString(kipiplugins_version));
            meta.setImageDimensions(QSize(width, height));
            meta.setExifTagString("Exif.Image.DocumentName", qfi.fileName());
            meta.setXmpTagString("Xmp.tiff.Make",  meta.getExifTagString("Exif.Image.Make"));
            meta.setXmpTagString("Xmp.tiff.Model", meta.getExifTagString("Exif.Image.Model"));
            meta.setImageOrientation(KExiv2Iface::KExiv2::ORIENTATION_NORMAL);

            QByteArray prof = KPWriteImage::getICCProfilFromFile(settings.outputColorSpace);

            KPWriteImage wImageIface;
            wImageIface.setImageData(imageData, width, height, true, false, prof, meta);
            QString outfname = QString(qfi.path() + QString("/.") + qfi.completeBaseName() + QString(".tiff"));

            if (!wImageIface.write2TIFF(outfname))
            {
                emit loadFailed(tr("ERROR: Cannot write target TIFF file decoded from RAW file: %1").arg(qfi.fileName()), image_idx);
                return;
            }
            else
            {
                qDebug("TH: Loading raw -> 16bit tiff file name = %s", qPrintable(outfname));
                TiffReader reader(QFile::encodeName(outfname).constData());
                pfs::Frame *frame = reader.readIntoPfsFrame();
                emit mdrReady(frame, image_idx, expotime, outfname);
                return;
            }

            // now do not remove tiff file, it might be required by align_image_stack
        }
    }
    catch (...)
    {
        qDebug("LIT: catched exception");
        emit loadFailed(QString(tr("ERROR: Failed Loading file: %1")).arg(fname), image_idx);
        return;
    }
}

} // namespace KIPIHDRCreatorPlugin
