/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2004 by Grzegorz Krawczyk <gkrawczyk at users dot sourceforge dot net>
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef _ROBERTSON02_H_
#define _ROBERTSON02_H_

// Qt includes

#include <QList>
#include <QImage>

// Local includes

#include "responses.h"

namespace KIPIHDRCreatorPlugin
{

/**
 * @brief Create HDR image by applying response curve to given images taken with different exposures
 *
 * @param xj [out] HDR image
 * @param imgs reference to vector containing source exposures
 * @param I camera response function (array size of M)
 * @param w weighting function for camera output values (array size of M)
 * @param M number of camera output levels
 * @return number of saturated pixels in the HDR image (0: all OK)
 */
void robertson02_applyResponse( pfs::Array2D* Rout,pfs::Array2D* Gout,pfs::Array2D* Bout, 
                                const float * arrayofexptime, const float* Ir, const float* Ig, 
                                const float* Ib, const float* w, const int M, const bool ldrinput, ... );

/**
 * @brief Calculate camera response using Robertson02 algorithm
 *
 * @param xj [out]  estimated luminance values
 * @param imgs reference to vector containing source exposures
 * @param I [out] array to put response function
 * @param w weights
 * @param M max camera output (no of discrete steps)
 * @return number of saturated pixels in the HDR image (0: all OK)
 */
void robertson02_getResponse( pfs::Array2D* Rout,pfs::Array2D* Gout,pfs::Array2D* Bout, 
                              const float * arrayofexptime, float* Ir,float* Ig,float* Ib, const float* w, 
                              const int M, const bool ldrinput, ... );

} // namespace KIPIHDRCreatorPlugin

#endif // _ROBERTSON02_H_ 
