/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef LOADHDRINPUT_H
#define LOADHDRINPUT_H

// Qt includes

#include <QThread>
#include <QImage>

// Local includes

#include "options.h"
#include "pfs.h"

namespace KIPIHDRCreatorPlugin
{

class hdrInputLoader : public QThread
{
    Q_OBJECT

public:

    hdrInputLoader(const QString& filename, int image_idx);
    ~hdrInputLoader();

Q_SIGNALS:

    void ldrReady(QImage *ldrImage, int index, float expotime, QString new_fname, bool ldrtiff);
    void mdrReady(pfs::Frame *mdrImage, int index, float expotime, QString new_fname);
    void loadFailed(QString errormessage, int index);

protected:

    void run();

private:

    int     image_idx;
    QString fname;
};

} // namespace KIPIHDRCreatorPlugin

#endif // LOADHDRINPUT_H
