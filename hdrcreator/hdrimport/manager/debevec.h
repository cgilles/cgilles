/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// Qt includes

#include <QList>
#include <QImage>

// Local includes

#include "responses.h"

namespace KIPIHDRCreatorPlugin
{

/**
 * @brief Create HDR image by applying response curve to given images using Debevec model, simple model, using array of weights P, not checking for under/over exposed pixel values.
 *
 * @param list reference to input Qt list containing source exposures, channels RGB
 * @param arrayofexptime array of floats containing equivalent exposure time (computed from time,f-value and ISO)
 * @param xj [out] HDR image channel 1
 * @param yj [out] HDR image channel 2
 * @param zj [out] HDR image channel 3
 * @param I1 response curve for channel 1, to be found with robertson02
 * @param I2 response curve for channel 2, to be found with robertson02
 * @param I3 response curve for channel 3, to be found with robertson02
 * @param P  width*height*#exposures array of weights
 */
/*
int debevec_applyResponse( const float *arrayofexptime,
            pfs::Array2D* xj, const float* I1,
            pfs::Array2D* yj, const float* I2,
            pfs::Array2D* zj, const float* I3,
            const Array2DList &P, const bool ldrinput, ... );
*/

/**
 * @brief Create HDR image by applying response curve to given images using Debevec model, checking for under/over exposed pixel values.
 *
 * @param list reference to input Qt list containing source exposures, channels RGB
 * @param arrayofexptime array of floats containing equivalent exposure time (computed from time,f-value and ISO)
 * @param xj [out] HDR image channel 1
 * @param yj [out] HDR image channel 2
 * @param zj [out] HDR image channel 3
 * @param Ir response curve for channel 1, to be found with robertson02
 * @param Ig response curve for channel 2, to be found with robertson02
 * @param Ib response curve for channel 3, to be found with robertson02
 * @param w  array of weights
 * @param M  lenght of w
 */
void debevec_applyResponse( const float * arrayofexptime,
               pfs::Array2D* xj, pfs::Array2D* yj, pfs::Array2D* zj,
               const float* Ir, const float* Ig, const float* Ib,
               const float *w, int M, const bool ldrinput, ... );

} // namespace KIPIHDRCreatorPlugin