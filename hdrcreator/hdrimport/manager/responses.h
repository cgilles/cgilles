/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2004 by Grzegorz Krawczyk <gkrawczyk at users dot sourceforge dot net>
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef _STD_RESPONSES_H_
#define _STD_RESPONSES_H_

// C++ includes

#include <cstdio>
#include <vector>

// Local includes

#include "array2d.h"

namespace KIPIHDRCreatorPlugin
{

/**
 * @brief Container for images taken with different exposures
 */
struct Exposure
{
  float ti;			// exposure value (eg time)
  pfs::Array2D* yi;		// exposed pixel value (camera output)
};


/**
 * @brief Container for a list of exposures
 */
typedef std::vector<Exposure> ExposureList;
typedef std::vector<pfs::Array2D*> Array2DList;

/**
 * @brief Weighting function with "flat" distribution (as in icip06)
 *
 * @param w [out] weights (array size of M)
 * @param M number of camera output levels
 */
void exposure_weights_icip06( float* w, int M, int Mmin, int Mmax );

/**
 * @brief Weighting function with triangle distribution (as in debevec)
 *
 * @param w [out] weights (array size of M)
 * @param M number of camera output levels
 */
void weights_triangle( float* w, int M/*, int Mmin, int Mmax */);

/**
 * @brief Weighting function with gaussian distribution
 *
 * @param w [out] weights (array size of M)
 * @param M number of camera output levels
 * @param Mmin minimum registered camera output level
 * @param Mmax maximum registered camera output level
 * @param sigma sigma value for gaussian
 */
void weightsGauss( float* w, int M, int Mmin, int Mmax, float sigma );

/**
 * @brief Create gamma response function
 *
 * @param I [out] camera response function (array size of M)
 * @param M number of camera output levels
 */
void responseGamma( float* I, int M );

/**
 * @brief Create linear response function
 *
 * @param I [out] camera response function (array size of M)
 * @param M number of camera output levels
 */
void responseLinear( float* I, int M );

/**
 * @brief Create logarithmic response function
 *
 * @param I [out] camera response function (array size of M)
 * @param M number of camera output levels
 */
void responseLog10( float* I, int M );

/**
 * @brief Save response curve to a MatLab file for further reuse
 *
 * @param file file handle to save response curve
 * @param I camera response function (array size of M)
 * @param M number of camera output levels
 * @param name matrix name for use in Octave or Matlab
 */
void responseSave( FILE* file, const float* I, int M, const char* name);

/**
 * @brief Save response curve to a MatLab file for further reuse
 *
 * @param file file handle to save response curve
 * @param w weights (array size of M)
 * @param M number of camera output levels
 * @param name matrix name for use in Octave or Matlab
 */
void weightsSave( FILE* file, const float* w, int M, const char* name);

/**
 * @brief Load response curve (saved with responseSave();)
 *
 * @param file file handle to save response curve
 * @param I [out] camera response function (array size of M)
 * @param M number of camera output levels
 * @return false means file has different output levels or is wrong for some other reason
 */
bool responseLoad( FILE* file, float* I, int M);

/**
 * @brief Load response curve (saved with responseSave();)
 *
 * @param file file handle to save response curve
 * @param w [out] weights (array size of M)
 * @param M number of camera output levels
 * @return false means file has different output levels or is wrong for some other reason
 */
bool weightsLoad( FILE* file, float* w, int M);

} // namespace KIPIHDRCreatorPlugin

#endif // _STD_RESPONSES_H_
