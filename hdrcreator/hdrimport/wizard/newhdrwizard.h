/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef HDRWIZARDFORM_IMPL_H
#define HDRWIZARDFORM_IMPL_H

// Qt includes

#include <QDialog>
#include <QString>
#include <QPixmap>

// KDE includes

#include <kurl.h>

// Local includes

#include "ui_hdrwizardform.h"
#include "options.h"
#include "gang.h"
#include "global.h"
#include "pfs.h"
#include "hdrcreationmanager.h"

namespace KIPI
{
    class Interface;
}

using namespace KIPI;

namespace KIPIHDRCreatorPlugin
{

class NewHdrWizard : public QDialog, private Ui::HdrWizardForm
{
    Q_OBJECT

public:

    NewHdrWizard(QWidget *parent, const KUrl::List& files, Interface* iface);
    ~NewHdrWizard();

    QString getCaptionTEXT();

Q_SIGNALS:

    void signalFinished(void* hdr);

protected:

    void resizeEvent(QResizeEvent*);
    void keyPressEvent(QKeyEvent*);
    virtual void dragEnterEvent(QDragEnterEvent*);
    virtual void dropEvent(QDropEvent*);

private Q_SLOTS:

    void fileLoaded(int index, QString fname, float expotime);
    void finishedLoadingInputFiles(QStringList NoExifFiles);
    void errorWhileLoading(QString errormessage);

    void updateGraphicalEVvalue(float expotime, int index_in_table);
    void finishedAligning();

    void loadImagesButtonClicked();
    void inputHdrFileSelected(int);
    void predefConfigsComboBoxActivated(int);
    void antighostRespCurveComboboxActivated(int);
    void customConfigCheckBoxToggled(bool);
    void triGaussPlateauComboBoxActivated(int);
    void predefRespCurveRadioButtonToggled(bool);
    void gammaLinLogComboBoxActivated(int);
    void loadRespCurveFromFileCheckboxToggled(bool);
    void loadRespCurveFileButtonClicked();
    void saveRespCurveToFileCheckboxToggled(bool);
    void saveRespCurveFileButtonClicked();
    void modelComboBoxActivated(int);
    void NextFinishButtonClicked();
    void currentPageChangedInto(int);
    void loadRespCurveFilename(const QString&);
    void editingEVfinished();

    void ais_failed(QProcess::ProcessError);

    void slotThumbnail(const KUrl&, const QPixmap& pix);

private:

    QString getQStringFromConfig( int type );
    void    loadInputFiles(const KUrl::List files, int count);

private:

    Gang*               EVgang;

    HdrCreationManager* hdrCreationManager;

    QString             loadcurvefilename, savecurvefilename;

    //hdr creation parameters
    TResponse           responses_in_gui[4];
    TModel              models_in_gui[2];
    TWeight             weights_in_gui[3];

    Interface*          m_interface;
};

} // namespace KIPIHDRCreatorPlugin

#endif // HDRWIZARDFORM_IMPL_H
