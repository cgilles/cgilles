/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef GLOBAL_H
#define GLOBAL_H

// Qt includes

#include <QSettings>

namespace KIPIHDRCreatorPlugin
{

extern QSettings s_settings;

bool        matchesLdrFilename(QString file);
bool        matchesHdrFilename(QString file);
bool        matchesValidHDRorLDRfilename(QString file);
QStringList convertUrlListToFilenameList(QList<QUrl> urls);
float       obtain_avg_lum(const QString& filename);

} // namespace KIPIHDRCreatorPlugin

#endif // GLOBAL_H
