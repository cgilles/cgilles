/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "paniconwidget.moc"

// C++ includes

#include <cmath>

// Qt includes

#include <QPainter>
#include <QPaintEvent>
#include <QDesktopWidget>
#include <QApplication>

namespace KIPIHDRCreatorPlugin
{

PanIconWidget::PanIconWidget(QWidget *parent, Qt::WindowFlags flags) : QFrame(parent,flags)
{
    setAttribute(Qt::WA_DeleteOnClose);
//     this->setFrameStyle(QFrame::Box|QFrame::Plain);
//     setLineWidth(1);
//     setMidLineWidth(2);
//     setContentsMargins(2,2,2,2);
    moveSelection = false;
    setMouseTracking(true);
}

void PanIconWidget::setImage(const QImage& fullsize)
{
    m_image     = QImage(fullsize.scaled(180, 120, Qt::KeepAspectRatio));
    m_width     = m_image.width();
    m_height    = m_image.height();
    m_orgWidth  = fullsize.width();
    m_orgHeight = fullsize.height();
    setFixedSize(m_width+2*frameWidth(), m_height+2*frameWidth());
//     m_rect = QRect(width()/2-m_width/2, height()/2-m_height/2, m_width, m_height);
    //KPopupFrame::setMainWidget = resize
    resize(m_width+2*frameWidth(), m_height+2*frameWidth());
//     qDebug("wanted w=%d, h=%d",m_width+2*frameWidth(), m_height+2*frameWidth());
}

void PanIconWidget::regionSelectionMoved(bool targetDone)
{
    int x = (int)lround( ((float)m_localRegionSelection.x() /*- (float)m_rect.x()*/ ) *
                         ((float)m_orgWidth / (float)m_width) );

    int y = (int)lround( ((float)m_localRegionSelection.y() /*- (float)m_rect.y()*/ ) *
                         ((float)m_orgHeight / (float)m_height) );

    int w = (int)lround( (float)m_localRegionSelection.width() *
                         ((float)m_orgWidth / (float)m_width) );

    int h = (int)lround( (float)m_localRegionSelection.height() *
                         ((float)m_orgHeight / (float)m_height) );

    regionSelection.setX(x);
    regionSelection.setY(y);
    regionSelection.setWidth(w);
    regionSelection.setHeight(h);

    update();
    emit signalSelectionMoved( regionSelection, targetDone );
}

void PanIconWidget::setMouseFocus()
{
    raise();
    xpos          = m_localRegionSelection.center().x();
    ypos          = m_localRegionSelection.center().y();
    moveSelection = true;
}

void PanIconWidget::setCursorToLocalRegionSelectionCenter(void)
{
    QCursor::setPos(mapToGlobal(m_localRegionSelection.center()));
}

void PanIconWidget::setRegionSelection(QRect rs)
{
    //rs is the rect of viewport (xy,wh) over entire image area (corrected considering zoom)
    //store it
    this->regionSelection = rs;
    //and now scale it to this widget's size/original_image_size
    m_localRegionSelection.setX( /*m_rect.x() +*/ (int)((float)rs.x() *
                    ((float)m_width / (float)m_orgWidth)) );

    m_localRegionSelection.setY( /*m_rect.y() +*/ (int)((float)rs.y() *
                    ((float)m_height / (float)m_orgHeight)) );

    m_localRegionSelection.setWidth( (int)((float)rs.width() *
                    ((float)m_width / (float)m_orgWidth)) );

    m_localRegionSelection.setHeight( (int)((float)rs.height() *
                    ((float)m_height / (float)m_orgHeight)) );
}

void PanIconWidget::mousePressEvent ( QMouseEvent * e )
{
    if ( (e->button() == Qt::LeftButton || e->button() == Qt::MidButton) &&
         m_localRegionSelection.contains( e->x(), e->y() ) )
    {
        xpos          = e->x();
        ypos          = e->y();
        moveSelection = true;
    }
}

void PanIconWidget::mouseMoveEvent ( QMouseEvent * e )
{
    if ( moveSelection &&
         (e->buttons() == Qt::LeftButton || e->buttons() == Qt::MidButton) )
    {
        int newxpos = e->x();
        int newypos = e->y();

        m_localRegionSelection.translate(newxpos - xpos, newypos - ypos);

        xpos = newxpos;
        ypos = newypos;

        // Perform normalization of selection area.

        if (m_localRegionSelection.left() < /*m_rect.left()*/0)
            m_localRegionSelection.moveLeft(/*m_rect.left()*/0);

        if (m_localRegionSelection.top() < /*m_rect.top()*/0)
            m_localRegionSelection.moveTop(/*m_rect.top()*/0);

        if (m_localRegionSelection.right() > /*m_rect.right()*/width())
            m_localRegionSelection.moveRight(/*m_rect.right()*/width());

        if (m_localRegionSelection.bottom() > /*m_rect.bottom()*/height())
            m_localRegionSelection.moveBottom(/*m_rect.bottom()*/height());

        update();
        regionSelectionMoved(false);
        return;
    }
    else
    {
        if ( m_localRegionSelection.contains( e->x(), e->y() ) )
            QApplication::setOverrideCursor( QCursor(Qt::PointingHandCursor));
        else
            QApplication::restoreOverrideCursor();
    }
}

void PanIconWidget::mouseReleaseEvent ( QMouseEvent * )
{
    if ( moveSelection )
    {
        moveSelection = false;
        QApplication::restoreOverrideCursor();
        regionSelectionMoved(true);
    }
}

PanIconWidget::~PanIconWidget()
{
}

void PanIconWidget::paintEvent(QPaintEvent *e)
{
    if (m_image.isNull())
        return;

    QPainter p(this);
    p.drawImage(e->rect(), m_image);

    p.setPen(QPen(Qt::white, 1, Qt::SolidLine));
    p.drawRect(m_localRegionSelection.x(),
               m_localRegionSelection.y(),
               m_localRegionSelection.width(),
               m_localRegionSelection.height());

    p.setPen(QPen(Qt::red, 1, Qt::DotLine));

    p.drawRect(m_localRegionSelection.x(),
               m_localRegionSelection.y(),
               m_localRegionSelection.width(),
               m_localRegionSelection.height());
}

void PanIconWidget::popup(const QPoint &pos)
{
    QRect d = QApplication::desktop()->screenGeometry();
    int x = pos.x();
    int y = pos.y();
    int w = width();
    int h = height();
    if (x+w > d.x()+d.width())
        x = d.width() - w;
    if (y+h > d.y()+d.height())
        y = d.height() - h;
    if (x < d.x())
        x = 0;
    if (y < d.y())
        y = 0;

    // Pop the thingy up.
    move(x, y);
    show();
}

} // namespace KIPIHDRCreatorPlugin
