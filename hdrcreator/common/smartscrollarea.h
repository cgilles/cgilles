/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef SMARTSSCROLLAREA_H
#define SMARTSSCROLLAREA_H

// Qt includes

#include <QScrollArea>
#include <QScrollBar>
#include <QLabel>
#include <QMouseEvent>

namespace KIPIHDRCreatorPlugin
{

class SmartScrollArea : public QScrollArea 
{
    Q_OBJECT
    
public:
  
    SmartScrollArea( QWidget *parent, QLabel *imagelabel );
    void zoomIn();
    void zoomOut();
    void fitToWindow(bool checked);
    void normalSize();
    void scaleLabelToFit();
    double getScaleFactor()
    {
        return scaleFactor;
    }
    bool isFitting()
    {
        return fittingwin;
    }
    
protected:

    void resizeEvent ( QResizeEvent * );
    void mousePressEvent(QMouseEvent *e) 
    {
        mousePos = e->globalPos();
    }
    void mouseMoveEvent(QMouseEvent *e);

private:

    void scaleImage(double);
    void adjustScrollBar(QScrollBar *scrollBar, double factor);

private:
  
    QLabel* imageLabel;
    QPoint  mousePos;
    double  scaleFactor;
    bool    fittingwin;
};

} // namespace KIPIHDRCreatorPlugin

#endif // SMARTSSCROLLAREA_H
