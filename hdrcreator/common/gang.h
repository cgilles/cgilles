/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef GANG_H
#define GANG_H

// Qt include

#include <QSlider>
#include <QDoubleSpinBox>

namespace KIPIHDRCreatorPlugin
{

class Gang : public QObject
{
    Q_OBJECT
    
public:
  
    Gang(QSlider* s_, QDoubleSpinBox* dsb_, 
        const float minv_, const float maxv_, 
        const float vv, const bool logs = false);
    
    float v() const { return v_; };
    void setDefault();
    bool changed() const { return changed_; };
    QString flag(const QString f) const;
    QString fname(const QString f) const;
    float p2v(const int p) const;
    int v2p(const float x) const;
    
public slots:
  
    void sliderMoved(int p);
    void sliderValueChanged(int p);
// 	void spinboxFocusEnter();
    void spinboxValueChanged(double);
    
signals:
  
    void finished();
    
private:
  
    QSlider*        s;
    QDoubleSpinBox* dsb;
    float           minv;
    float           maxv;
    float           defaultv;
    bool            logscaling;
    float           v_;
    bool            value_from_text;
    bool            value_from_slider;
    bool            graphics_only;
    bool            changed_;
};

} // namespace KIPIHDRCreatorPlugin

#endif // GANG_H
