/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// C++ includes

#include <cmath>

// Qt includes

#include <QImage>
#include <QPointer>
#include <QUrl>

// KDE includes

#include <kdebug.h>

// Libkexiv2 includes

#include <libkexiv2/kexiv2.h>

// Local includes

#include "configuration.h"
#include "options.h"
#include "global.h"

using namespace KExiv2Iface;

namespace KIPIHDRCreatorPlugin
{

QSettings s_settings("Qtpfsgui", "Qtpfsgui");

bool matchesLdrFilename(QString file)
{
    QRegExp exp(".*\\.(jpeg|jpg|tiff|tif|crw|cr2|nef|dng|mrw|orf|kdc|dcr|arw|raf|ptx|pef|x3f|raw|sr2)$", Qt::CaseInsensitive);
    return exp.exactMatch(file);
}

bool matchesHdrFilename(QString file)
{
    QRegExp exp(".*\\.(exr|hdr|pic|tiff|tif|pfs|crw|cr2|nef|dng|mrw|orf|kdc|dcr|arw|raf|ptx|pef|x3f|raw|sr2)$", Qt::CaseInsensitive);
    return exp.exactMatch(file);
}

bool matchesValidHDRorLDRfilename(QString file)
{
    return matchesLdrFilename(file) || matchesHdrFilename(file);
}

QStringList convertUrlListToFilenameList(QList<QUrl> urls)
{
    QStringList files;
    for (int i = 0; i < urls.size(); ++i)
    {
        QString localFile = urls.at(i).toLocalFile();
        if (matchesValidHDRorLDRfilename(localFile))
            files.append(localFile);
    }
    return files;
}

/**
 * This function obtains the "average scene luminance" (cd/m^2) from an image file.
 * "average scene luminance" is the L (aka B) value mentioned in [1]
 * You have to take a log2f of the returned value to get an EV value.
 *
 * We are using K=12.07488f and the exif-implied value of N=1/3.125 (see [1]).
 * K=12.07488f is the 1.0592f * 11.4f value in pfscalibration's pfshdrcalibrate.cpp file.
 * Based on [3] we can say that the value can also be 12.5 or even 14.
 * Another reference for APEX is [4] where N is 0.3, closer to the APEX specification of 2^(-7/4)=0.2973.
 *
 * [1] http://en.wikipedia.org/wiki/APEX_system
 * [2] http://en.wikipedia.org/wiki/Exposure_value
 * [3] http://en.wikipedia.org/wiki/Light_meter
 * [4] http://doug.kerr.home.att.net/pumpkin/#APEX
 *
 * This function tries first to obtain the shutter speed from either of
 * two exif tags (there is no standard between camera manifacturers):
 * ExposureTime or ShutterSpeedValue.
 * Same thing for f-number: it can be found in FNumber or in ApertureValue.
 *
 * F-number and shutter speed are mandatory in exif data for EV calculation, iso is not.
 */
float obtain_avg_lum(const QString& filename)
{
    KExiv2 meta;
    meta.load(filename);
    if (!meta.hasExif())
        return -1;

    long int num = 1, den = 1;

    // default not valid values

    float expo = -1;
    float iso  = -1;
    float fnum = -1;

    if (meta.getExifTagRational("Exif.Photo.ExposureTime", num, den))
    {
        expo = (float)(num) / (float)(den);
    }
    else if (meta.getExifTagRational("Exif.Photo.ShutterSpeedValue", num, den))
    {
        long   nmr = 1, div = 1;
        double tmp = exp(log(2.0) * (float)(num) / (float)(den));

        if (tmp > 1)
        {
            div = (long)(tmp + 0.5);
        }
        else
        {
            nmr = (long)(1 / tmp + 0.5);
        }

        expo = (float)(nmr) / (float)(div);
    }

    if (meta.getExifTagRational("Exif.Photo.FNumber", num, den))
    {
        fnum = (float)(num) / (float)(den);
    }
    else if (meta.getExifTagRational("Exif.Photo.ApertureValue", num, den))
    {
        fnum = (float)(exp(log(2.0) * (float)(num) / (float)(den) / 2.0));
    }

    // Some cameras/lens DO print the fnum but with value 0, and this is not allowed for ev computation purposes.

    if (fnum == 0)
        return -1;

    // If iso is found use that value, otherwise assume a value of iso=100. (again, some cameras do not print iso in exif).

    if (meta.getExifTagRational("Exif.Photo.ISOSpeedRatings", num, den))
    {
        iso = (float)(num) / (float)(den);
    }
    else
    {
        iso = 100.0;
    }

    // At this point the three variables have to be != -1

    if (expo != -1 && iso != -1 && fnum != -1)
    {
        kDebug() << "expo=" << expo << " fnum=" << fnum << " iso=" << iso << " |returned=" << (expo * iso) / (fnum * fnum * 12.07488f);
        return ( (expo * iso) / (fnum * fnum * 12.07488f) );
    }

    return -1;
}

} // namespace KIPIHDRCreatorPlugin
