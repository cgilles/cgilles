/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "ldrviewer.moc"

// Qt includes

#include <QVBoxLayout>

// KDE includes

#include <kiconloader.h>

// Local includes

#include "configuration.h"

namespace KIPIHDRCreatorPlugin
{

LdrViewer::LdrViewer(QWidget *parent, const TMImg& data, tonemapping_options *opts)
         : QWidget(parent), origimage(data)
{
    setAttribute(Qt::WA_DeleteOnClose);

    QVBoxLayout *VBL_L = new QVBoxLayout(this);
    VBL_L->setSpacing(0);
    VBL_L->setMargin(0);

    imageLabel = new QLabel;
    imageLabel->setPixmap(QPixmap::fromImage(origimage.toQImage()));
    scrollArea = new SmartScrollArea(this, imageLabel);
    VBL_L->addWidget(scrollArea);

    parseOptions(opts);
    setWindowTitle(caption);
    setToolTip(caption);
    cornerButton = new QToolButton(this);
    cornerButton->setToolTip(i18n("Pan the image to a region"));
    cornerButton->setIcon(SmallIcon("transform-move"));
    scrollArea->setCornerWidget(cornerButton);

    connect(cornerButton, SIGNAL(pressed()),
            this, SLOT(slotCornerButtonPressed()));
}

LdrViewer::~LdrViewer()
{
}

void LdrViewer::slotCornerButtonPressed()
{
    panIconWidget     = new PanIconWidget;
    panIconWidget->setImage(origimage.toQImage());
    float zf          = scrollArea->getScaleFactor();
    float leftviewpos = (float)(scrollArea->horizontalScrollBar()->value());
    float topviewpos  = (float)(scrollArea->verticalScrollBar()->value());
    float wps_w       = (float)(scrollArea->maximumViewportSize().width());
    float wps_h       = (float)(scrollArea->maximumViewportSize().height());
    QRect r((int)(leftviewpos/zf), (int)(topviewpos/zf), (int)(wps_w/zf), (int)(wps_h/zf));
    panIconWidget->setRegionSelection(r);
    panIconWidget->setMouseFocus();

    connect(panIconWidget, SIGNAL(signalSelectionMoved(QRect, bool)),
            this, SLOT(slotPanIconSelectionMoved(QRect, bool)));

    QPoint g = scrollArea->mapToGlobal(scrollArea->viewport()->pos());
    g.setX(g.x()+ scrollArea->viewport()->size().width());
    g.setY(g.y()+ scrollArea->viewport()->size().height());
    panIconWidget->popup(QPoint(g.x() - panIconWidget->width()/2,
                         g.y() - panIconWidget->height()/2));

    panIconWidget->setCursorToLocalRegionSelectionCenter();
}

void LdrViewer::slotPanIconSelectionMoved(QRect gotopos, bool mousereleased)
{
    if (mousereleased)
    {
        scrollArea->horizontalScrollBar()->setValue((int)(gotopos.x()*scrollArea->getScaleFactor()));
        scrollArea->verticalScrollBar()->setValue((int)(gotopos.y()*scrollArea->getScaleFactor()));
        panIconWidget->close();
        slotPanIconHidden();
    }
}

void LdrViewer::slotPanIconHidden()
{
    cornerButton->blockSignals(true);
    cornerButton->animateClick();
    cornerButton->blockSignals(false);
}

void LdrViewer::parseOptions(tonemapping_options *opts)
{
    TMOptionsOperations tmopts(opts);
    postfix      = tmopts.getPostfix();
    caption      = tmopts.getCaption();
    exif_comment = tmopts.getExifComment();
}

QString LdrViewer::getFilenamePostFix() const
{
    return postfix;
}

void LdrViewer::slotFitToWindow(bool checked)
{
    scrollArea->fitToWindow(checked);
}

bool LdrViewer::getFittingWin()
{
    return scrollArea->isFitting();
}

TMImg LdrViewer::getTMImage() const
{
    return origimage;
}

QString LdrViewer::getExifComment() const
{
    return exif_comment;
}

} // namespace KIPIHDRCreatorPlugin
