/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef TONEMAPPINGWIDGET_H
#define TONEMAPPINGWIDGET_H

// Qt includes

#include <QProgressBar>

// Local includes

#include "ui_tonemappingoptions.h"
#include "gang.h"
#include "global.h"
#include "options.h"
#include "pfs.h"
#include "tonemapperthread.h"

class QStatusBar;

namespace KIPIHDRCreatorPlugin
{

class MyProgressBar : public QProgressBar
{
    Q_OBJECT

public:

    MyProgressBar(QWidget* parent = 0);
    ~MyProgressBar();

Q_SIGNALS:

    void leftMouseButtonClicked();

public Q_SLOTS:

    void advanceCurrentProgress();

protected:

    void mousePressEvent(QMouseEvent *event);
};

// -----------------------------------------------------------------------------------------

class TMWidget : public QWidget, public Ui::ToneMappingOptions
{
    Q_OBJECT

public:

    TMWidget(QWidget *parent, pfs::Frame* frame, QStatusBar* sb);
    ~TMWidget();

Q_SIGNALS:

    void signalNewResult(const KIPIHDRCreatorPlugin::TMImg&, tonemapping_options*);

private Q_SLOTS:

    void on_pregammadefault_clicked();
    void on_ashikhmin02Default_clicked();
    void on_drago03Default_clicked();
    void on_durand02Default_clicked();
    void on_fattal02Default_clicked();
    void on_pattanaik00Default_clicked();
    void on_reinhard02Default_clicked();
    void on_reinhard05Default_clicked();
    void on_MantiukDefault_clicked();
    void on_applyButton_clicked();
    void on_savesettingsbutton_clicked();
    void on_loadsettingsbutton_clicked();

    //APPLY tmo settings from text file
    void fromTxt2Gui();

    //user wants a custom size.
    void on_addCustomSizeButton_clicked();
    void fillCustomSizeComboBox();

private:

    void FillToneMappingOptions();

    // i.e. WRITE tmo settings to text file
    void fromGui2Txt(QString destination);

private:

    QVector<int> sizes;

    Gang *contrastfactorGang;
    Gang *saturationfactorGang;
    Gang *detailfactorGang;
    Gang *contrastGang;
    Gang *biasGang;
    Gang *spatialGang;
    Gang *rangeGang;
    Gang *baseGang;
    Gang *alphaGang;
    Gang *betaGang;
    Gang *saturation2Gang;
    Gang *noiseGang;
    Gang *multiplierGang;
    Gang *coneGang;
    Gang *rodGang;
    Gang *keyGang;
    Gang *phiGang;
    Gang *range2Gang;
    Gang *lowerGang;
    Gang *upperGang;
    Gang *brightnessGang;
    Gang *chromaticGang;
    Gang *lightGang;
    Gang *pregammagang;

    tonemapping_options  ToneMappingOptions;
    pfs::Frame*          OriginalPfsFrame;
    QString              RecentPathLoadSaveTmoSettings;
    QString              TMOSettingsFilename;
    QString              cachepath;
    int                  out_ldr_cs;
    QStatusBar*          sb;
    bool                 adding_custom_size;
    float                HeightWidthRatio;
};

} // namespace KIPIHDRCreatorPlugin

#endif // TONEMAPPINGWIDGET_H
