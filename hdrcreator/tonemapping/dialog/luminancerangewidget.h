/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef LUMINANCERANGE_WIDGET_H
#define LUMINANCERANGE_WIDGET_H

// Qt includes

#include <QFrame>

namespace pfs 
{
    class Array2D;
    class Frame;
}

namespace KIPIHDRCreatorPlugin
{

class Histogram;

class LuminanceRangeWidget : public QFrame
{
  Q_OBJECT

public:

    LuminanceRangeWidget(pfs::Frame* frame, QWidget *parent=0);
    ~LuminanceRangeWidget();

    QSize sizeHint () const;

    float getRangeWindowMin() const
    {
        return windowMin;
    }

    float getRangeWindowMax() const
    {
        return windowMax;
    }

    void setRangeWindowMinMax( float min, float max );
    
    void showValuePointer( float value );
    void hideValuePointer();
    
protected:

    void paintEvent( QPaintEvent * );
    void mouseMoveEvent( QMouseEvent * );
    void mousePressEvent( QMouseEvent * me );
    void mouseReleaseEvent( QMouseEvent * me );

    float draggedMin();
    float draggedMax();
  
Q_SIGNALS:

    void updateRangeWindow();

public Q_SLOTS:
  
    void decreaseExposure();
    void increaseExposure();
    void extendRange();
    void shrinkRange();
    void fitToDynamicRange();
    void lowDynamicRange();

private:
    
    QRect getPaintRect() const;
    void computeHistogram();
    
private:

    enum DragMode 
    {
        DRAG_MIN, 
        DRAG_MAX, 
        DRAG_MINMAX, 
        DRAG_NO
    };  
    DragMode dragMode;
    
    float            minValue;
    float            maxValue;

    float            windowMin;
    float            windowMax;

    static const int DRAGNOTSTARTED = -1;
    int              mouseDragStart;
    float            dragShift;
    
    bool             showVP;
    float            valuePointer;

    Histogram*       histogram;
    pfs::Array2D*    histogramImage;
};

}  // namespace KIPIHDRCreatorPlugin

#endif // LUMINANCERANGE_WIDGET_H
