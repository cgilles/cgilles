/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef TONEMAPPERTHREAD_H
#define TONEMAPPERTHREAD_H

// Qt includes

#include <QThread>
#include <QReadWriteLock>
#include <QByteArray>
#include <QImage>

// Local includes

#include "options.h"
#include "global.h"
#include "pfs.h"

class QProgressBar;

namespace KIPIHDRCreatorPlugin
{

class TMImg
{
public:

    TMImg()
    {
        width  = 0;
        height = 0;
    }

    ~TMImg(){}

    TMImg& operator=(const TMImg& img)
    {
        width  = img.width;
        height = img.height;
        data   = img.data;
        return *this;
    }

    QSize size() const
    {
        return (QSize(width, height));
    }

    QImage toQImage() const
    {
        if (data.isEmpty()) return QImage();

        QImage img(width, height, QImage::Format_ARGB32);
        uint*  dptr            = (uint*)img.bits();
        unsigned short* sptr16 = (unsigned short*)data.data();

        for (int i = 0 ; i < width * height ; i++)
        {
            *dptr++ = qRgba((uchar)((sptr16[2] * 255UL)/65535UL),
                            (uchar)((sptr16[1] * 255UL)/65535UL),
                            (uchar)((sptr16[0] * 255UL)/65535UL),
                            0xFF);
            sptr16 += 3;
        }
        return img;
    }

public:

    int        width;
    int        height;

    QByteArray data;
};

// -------------------------------------------------------------------------

class TonemapperThread : public QThread
{
    Q_OBJECT

public:

    // tonemapping_options passed by value, bit-copy should be enough
    TonemapperThread(int origsize, const tonemapping_options opts);
    ~TonemapperThread();

public Q_SLOTS:

    void terminateRequested();

Q_SIGNALS:

    void signalImageComputed(const KIPIHDRCreatorPlugin::TMImg&, tonemapping_options*);
    void setMaximumSteps(int);
    void advanceCurrentProgress();

protected:

    void run();

private:

    enum
    {
        from_resize,
        from_pregamma,
        from_tm
    } status;

private:

    void fetch(QString);
    void swap(pfs::Frame*, QString);
    bool forciblyTerminated;

    TMImg fromLDRPFSto16Bits(pfs::Frame* inpfsframe);

private:

    int                 originalxsize;
    int                 ldr_output_cs;
    QString             cachepath;
    bool                colorspaceconversion;
    tonemapping_options opts;
    pfs::Frame*         workingframe;
};

} // namespace KIPIHDRCreatorPlugin

Q_DECLARE_METATYPE(KIPIHDRCreatorPlugin::TMImg)

#endif // TONEMAPPERTHREAD_H
