/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef HDR_VIEWER_H
#define HDR_VIEWER_H

// Qt includes

#include <QImage>
#include <QComboBox>
#include <QLabel>
#include <QResizeEvent>
#include <QToolButton>

// Local includes

#include "pfs.h"
#include "array2d.h"
#include "smartscrollarea.h"
#include "paniconwidget.h"
#include "luminancerangewidget.h"

namespace KIPIHDRCreatorPlugin
{
  
enum LumMappingMethod
{
    MAP_LINEAR,
    MAP_GAMMA1_4,
    MAP_GAMMA1_8,
    MAP_GAMMA2_2,
    MAP_GAMMA2_6,
    MAP_LOGARITHMIC
};

class HdrViewer : public QWidget
{
   Q_OBJECT

public:

    HdrViewer(QWidget* parent, pfs::Frame* frame, unsigned int negcol, unsigned int naninfcol);
    ~HdrViewer();

    double getScaleFactor();
    bool   getFittingWin();

    void update_colors(unsigned int negcol, unsigned int naninfcol);

public slots:

    void zoomIn();
    void zoomOut();
    void fitToWindow(bool checked);
    void normalSize();
    void updateRangeWindow();
    void setLumMappingMethod(int method);

private slots:

    void slotPanIconSelectionMoved(QRect, bool);
    void slotPanIconHidden();
    void slotCornerButtonPressed();

private:

    void          setRangeWindow( float min, float max );
    pfs::Array2D *getPrimaryChannel();
    void          updateImage();
    void          mapFrameToImage();

private:

    unsigned int          naninfcol;
    unsigned int          negcol;

    float                 minValue;
    float                 maxValue;

    QImage                image;

    QLabel*               imageLabel;

    QToolButton*          cornerButton;

    QComboBox*            mappingMethodCB;

    LuminanceRangeWidget* lumRange;

    SmartScrollArea*      scrollArea;

    PanIconWidget*        panIconWidget;

    pfs::Array2D*         workArea[3];
    pfs::Frame*           pfsFrame;

    LumMappingMethod      mappingMethod;
};

}  // namespace KIPIHDRCreatorPlugin

#endif // HDR_VIEWER_H
