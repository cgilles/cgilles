/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "hdrviewer.moc"

// C++ includes

#include <cmath>

// Qt includes

#include <QApplication>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileInfo>

// KDE includes

#include <klocale.h>
#include <kiconloader.h>
#include <khbox.h>
#include <kvbox.h>
#include <kdialog.h>

// Local includes

#include "global.h"

namespace KIPIHDRCreatorPlugin
{

inline float clamp( float val, float min, float max )
{
    if( val < min ) return min;
    if( val > max ) return max;
    return val;
}

inline int clamp( int val, int min, int max )
{
    if( val < min ) return min;
    if( val > max ) return max;
    return val;
}

static float getInverseMapping( LumMappingMethod mappingMethod, float v, float minValue, float maxValue )
{
    switch( mappingMethod )
    {
        case MAP_GAMMA1_4:
            return powf( v, 1.4 )*(maxValue-minValue) + minValue;
        case MAP_GAMMA1_8:
            return powf( v, 1.8 )*(maxValue-minValue) + minValue;
        case MAP_GAMMA2_2:
            return powf( v, 2.2 )*(maxValue-minValue) + minValue;
        case MAP_GAMMA2_6:
            return powf( v, 2.6 )*(maxValue-minValue) + minValue;
        case MAP_LINEAR:
            return v*(maxValue-minValue) + minValue;
        case MAP_LOGARITHMIC:
            return powf( 10, v * (log10f(maxValue) - log10f(minValue)) + log10f( minValue ) );
    }

    return 0;
}

inline int binarySearchPixels( float x, const float *lut, const int lutSize )
{
    int l = 0, r = lutSize;

    while( true ) 
    {
        int m = (l+r)/2;

        if( m == l ) break;

        if( x < lut[m] )
            r = m;
        else
            l = m;
    }

    return l;
}

// --------------------------------------------------------------------------------------

HdrViewer::HdrViewer(QWidget *parent, pfs::Frame* frame, unsigned int neg, unsigned int naninf) 
         : QWidget(parent),
           naninfcol(naninf), negcol(neg),
           minValue(1.0f), maxValue(1.0f),
           pfsFrame(frame),
           mappingMethod(MAP_GAMMA2_2)
{
    workArea[0] = 0;
    workArea[1] = 0;
    workArea[2] = 0;

    setAttribute(Qt::WA_DeleteOnClose);

    QVBoxLayout *vlay = new QVBoxLayout(this);
    KHBox *hbox       = new KHBox(this);
    KVBox *vbox       = new KVBox(hbox);
    QLabel *mapLabel  = new QLabel(vbox);
    mapLabel->setText( i18n("Mapping:") );

    vbox->setSpacing(KDialog::marginHint());
    vbox->setMargin(KDialog::marginHint());
    hbox->setSpacing(KDialog::marginHint());
    hbox->setMargin(KDialog::marginHint());

    mappingMethodCB   = new QComboBox( vbox );
    QStringList methods;
    methods << i18n("Linear");
    methods << i18n("Gamma 1.4");
    methods << i18n("Gamma 1.8");
    methods << i18n("Gamma 2.2");
    methods << i18n("Gamma 2.6");
    methods << i18n("Logarithmic");
    mappingMethodCB->addItems(methods);
    mappingMethodCB->setCurrentIndex(3);

    lumRange      = new LuminanceRangeWidget(pfsFrame, hbox);

    imageLabel    = new QLabel;
    scrollArea    = new SmartScrollArea(this, imageLabel);
    cornerButton  = new QToolButton(this);
    cornerButton->setToolTip(i18n("Pan the image to a region"));
    cornerButton->setIcon(SmallIcon("transform-move"));
    scrollArea->setBackgroundRole(QPalette::Shadow);
    scrollArea->setCornerWidget(cornerButton);

    vlay->addWidget(hbox);
    vlay->addWidget(scrollArea);
    vlay->setSpacing(KDialog::marginHint());
    vlay->setMargin(KDialog::marginHint());

    updateRangeWindow();

    // LuminanceRangeWidget::fitToDynamicRange() already takes care -indirectly- to call updateImage()
    // zoom at original size, 100%
    // make the label use the image dimensions
    imageLabel->adjustSize();
    imageLabel->update();

    connect(mappingMethodCB, SIGNAL( activated( int ) ), 
            this, SLOT( setLumMappingMethod(int) ) );

    connect(lumRange, SIGNAL( updateRangeWindow() ), 
            this, SLOT( updateRangeWindow() ) );

    connect(cornerButton, SIGNAL(pressed()), 
            this, SLOT(slotCornerButtonPressed()));
}

HdrViewer::~HdrViewer()
{
    // do not delete workarea, it shares the same memory area of pfsFrame
}

void HdrViewer::slotCornerButtonPressed()
{
    panIconWidget     = new PanIconWidget;
    panIconWidget->setImage(image);
    float zf          = scrollArea->getScaleFactor();
    float leftviewpos = (float)(scrollArea->horizontalScrollBar()->value());
    float topviewpos  = (float)(scrollArea->verticalScrollBar()->value());
    float wps_w       = (float)(scrollArea->maximumViewportSize().width());
    float wps_h       = (float)(scrollArea->maximumViewportSize().height());
    QRect r((int)(leftviewpos/zf), (int)(topviewpos/zf), (int)(wps_w/zf), (int)(wps_h/zf));
    panIconWidget->setRegionSelection(r);
    panIconWidget->setMouseFocus();

    connect(panIconWidget, SIGNAL(signalSelectionMoved(QRect, bool)), 
            this, SLOT(slotPanIconSelectionMoved(QRect, bool)));

    QPoint g = scrollArea->mapToGlobal(scrollArea->viewport()->pos());
    g.setX(g.x()+ scrollArea->viewport()->size().width());
    g.setY(g.y()+ scrollArea->viewport()->size().height());
    panIconWidget->popup(QPoint(g.x() - panIconWidget->width()/2,
                         g.y() - panIconWidget->height()/2));

    panIconWidget->setCursorToLocalRegionSelectionCenter();
}

void HdrViewer::slotPanIconSelectionMoved(QRect gotopos, bool mousereleased)
{
    if (mousereleased) 
    {
        scrollArea->horizontalScrollBar()->setValue((int)(gotopos.x()*scrollArea->getScaleFactor()));
        scrollArea->verticalScrollBar()->setValue((int)(gotopos.y()*scrollArea->getScaleFactor()));
        panIconWidget->close();
        slotPanIconHidden();
    }
}

void HdrViewer::slotPanIconHidden()
{
    cornerButton->blockSignals(true);
    cornerButton->animateClick();
    cornerButton->blockSignals(false);
}

void HdrViewer::updateImage()
{
    if (!pfsFrame) return;

    QApplication::setOverrideCursor( QCursor(Qt::WaitCursor) );

    pfs::Channel *R=0, *G=0, *B=0;
    pfsFrame->getRGBChannels( R, G, B );
    if(!R && !G && !B) return;

    // workarea needed in updateMapping(...) called by mapFrameToImage(...)

    workArea[0] = R;
    workArea[1] = G;
    workArea[2] = B;
    if(!workArea[0] && !workArea[1] && !workArea[2]) return;

    int zoomedWidth  = workArea[0]->getCols();
    int zoomedHeight = workArea[0]->getRows();
    image            = QImage(zoomedWidth, zoomedHeight, QImage::Format_RGB32 );

    mapFrameToImage();

    // assign the mapped image to the label

    imageLabel->setPixmap(QPixmap::fromImage(image));
    QApplication::restoreOverrideCursor();
}

void HdrViewer::mapFrameToImage()
{
    int rows     = workArea[0]->getRows();
    int cols     = workArea[0]->getCols();
    int index    = 0;
    float imgRow = 0, imgCol;
    float lutPixFloor[257*2];
    QRgb lutPixel[257*2];
    int lutSize  = 258;

    for( int p = 1; p <= 257; p++ )
    {
        float p_left   = ((float)p - 1.f)/255.f; // Should be -1.5f, but we don't want negative nums
        lutPixFloor[p] = getInverseMapping( mappingMethod, p_left, minValue, maxValue );
    }

    lutPixel[0]   = QColor( 0, 0, 0 ).rgb();
    lutPixel[257] = QColor( 255, 255, 255 ).rgb();
    int  pr, pg, pb;
    QRgb pixel;

    (void)lutPixel;

    for( int r = 0; r < rows; r++, imgRow++ ) 
    {
        QRgb* line = (QRgb*)image.scanLine( (int)imgRow );
        imgCol     = 0;

        for( int c = 0; c < cols; c++, index++, imgCol++ ) 
        {
            // Color channels
            pr = binarySearchPixels( (*workArea[0])(index), lutPixFloor, lutSize );
            pg = binarySearchPixels( (*workArea[1])(index), lutPixFloor, lutSize );
            pb = binarySearchPixels( (*workArea[2])(index), lutPixFloor, lutSize );

            // Clipping
            pixel = QColor( clamp( pr-1, 0, 255 ),
                            clamp( pg-1, 0, 255 ),
                            clamp( pb-1, 0, 255 ) ).rgb();

            if( !finite( (*workArea[0])(index) ) || !finite( (*workArea[1])(index) ) || 
                !finite( (*workArea[2])(index) ) )
            {
                // x is NaN or Inf
                pixel = naninfcol;
            }

            if( (*workArea[0])(index)<0 || (*workArea[1])(index)<0 || (*workArea[2])(index)<0 ) 
            {
                // x is negative
                pixel = negcol;
            }

            line[(int)imgCol] = pixel;
        }
    }
}

void HdrViewer::updateRangeWindow()
{
    setRangeWindow( pow( 10, lumRange->getRangeWindowMin() ), pow( 10, lumRange->getRangeWindowMax() ) );
}

void HdrViewer::setRangeWindow( float min, float max )
{
    minValue = min;
    maxValue = max;
    updateImage();
}

void HdrViewer::setLumMappingMethod( int method )
{
    mappingMethodCB->setCurrentIndex( method );
    mappingMethod = (LumMappingMethod)method;
    updateImage();
}

void HdrViewer::zoomIn()
{
    scrollArea->zoomIn();
}

void HdrViewer::zoomOut()
{
    scrollArea->zoomOut();
}

void HdrViewer::fitToWindow(bool checked)
{
    scrollArea->fitToWindow(checked);
}

void HdrViewer::normalSize()
{
    scrollArea->normalSize();
}

double HdrViewer::getScaleFactor()
{
    return scrollArea->getScaleFactor();
}

bool HdrViewer::getFittingWin()
{
    return scrollArea->isFitting();
}

void HdrViewer::update_colors( unsigned int neg, unsigned int naninf ) 
{
    naninfcol = naninf;
    negcol    = neg;
    updateImage();
}

pfs::Array2D* HdrViewer::getPrimaryChannel()
{
    if ( !pfsFrame) return 0;

    pfs::Channel *R=0, *G=0, *B=0;
    pfsFrame->getRGBChannels( R, G, B );
    return G;
}

}  // namespace KIPIHDRCreatorPlugin
