/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef LDR_VIEWER_H
#define LDR_VIEWER_H

// Qt includes

#include <QToolButton>

// Local includes

#include "smartscrollarea.h"
#include "paniconwidget.h"
#include "tonemappingwidget.h"

namespace KIPIHDRCreatorPlugin
{

class LdrViewer : public QWidget
{
    Q_OBJECT

public:

    LdrViewer(QWidget *parent, const TMImg&, tonemapping_options*);
    ~LdrViewer();

    bool         getFittingWin();
    QString      getFilenamePostFix() const;
    QString      getExifComment() const;
    TMImg        getTMImage() const;

public Q_SLOTS:

    void slotFitToWindow(bool checked);

private Q_SLOTS:

    void slotPanIconSelectionMoved(QRect, bool);
    void slotPanIconHidden();
    void slotCornerButtonPressed();

private:

    void parseOptions(tonemapping_options* opts);

private:

    QString          caption;
    QString          postfix;
    QString          exif_comment;

    QLabel*          imageLabel;
    QToolButton*     cornerButton;

    SmartScrollArea* scrollArea;
    TMImg            origimage;

    PanIconWidget*   panIconWidget;
};

} // namespace KIPIHDRCreatorPlugin

#endif // LDR_VIEWER_H
