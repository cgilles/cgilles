/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef TONEMAPPINGDIALOG_IMPL_H
#define TONEMAPPINGDIALOG_IMPL_H

// Qt includes

#include <QMdiArea>

// Local includes

#include "ui_tonemappingdialog.h"
#include "global.h"
#include "tonemappingwidget.h"

namespace KIPI
{
    class Interface;
}

using namespace KIPI;

namespace KIPIHDRCreatorPlugin
{

class SaveLdrImgThread;
class HdrViewer;

class TonemappingWindow : public QMainWindow, public Ui::TonemappingWindow
{
    Q_OBJECT

public:

    TonemappingWindow(QWidget* parent, pfs::Frame* frame, const QString& prefixname, Interface* iface);
    ~TonemappingWindow();

private Q_SLOTS:

    void slotAddMDIresult(const KIPIHDRCreatorPlugin::TMImg&, tonemapping_options*);
    void updateActions(QMdiSubWindow*);
    void viewAllAsThumbnails();
    void current_ldr_fit_to_win(bool);
    void on_actionClose_All_triggered();
    void on_actionSaveAll_triggered();
    void on_actionSave_triggered();
    void enterWhatsThis();
    void slotThreadDone(const KUrl::List& complete, const KUrl::List& failed);

private:

    void saveOneLdrImage(const QString& initialFileName, const TMImg& image, const QString& comment);

private:

    QMdiArea*         mdiArea;
    QString           recentPathSaveLDR;
    QString           prefixname;

    SaveLdrImgThread* m_saveThread;
    HdrViewer*        m_hdrViewver;
    
    Interface*        m_interface;
};

} // namespace KIPIHDRCreatorPlugin

#endif // TONEMAPPINGDIALOG_IMPL_H
