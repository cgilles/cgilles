/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-12-05
 * Description : save LDR image thread
 *
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#ifndef SAVELDRIMGTHREAD_H
#define SAVELDRIMGTHREAD_H

// Qt includes

#include <QObject>
#include <QThread>
#include <QString>
#include <QMap>

// KDE includes

#include <kurl.h>

// local includes

#include "tonemapperthread.h"

namespace KIPIHDRCreatorPlugin
{

class SaveLdrImgThreadPriv;

class SaveLdrImgThread : public QThread
{
    Q_OBJECT

public:

    SaveLdrImgThread(QObject *parent);
    ~SaveLdrImgThread();

    void setFormat(const QString& format);
    void setImagesMap(const QMap<KUrl, TMImg>& map);
    void setCommentsMap(const QMap<KUrl, QString>& map);

Q_SIGNALS:

    /** This signal fire a list of completed and failed url.
     */
    void signalComplete(const KUrl::List&, const KUrl::List&);

private:

    void run();
    bool saveOneImage(const KUrl& url, const TMImg& img, const QString& comments);

private:

    SaveLdrImgThreadPriv* const d;
};

}  // namespace KIPIHDRCreatorPlugin

#endif // SAVELDRIMGTHREAD_H
