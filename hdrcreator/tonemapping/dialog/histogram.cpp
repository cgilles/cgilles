/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

// C++ includes

#include <cmath>
#include <iostream>

// Local includes

#include "histogram.h"

namespace KIPIHDRCreatorPlugin
{
  
Histogram::Histogram( int bins, int accuracy )
         : bins( bins ), accuracy( accuracy )
{
    P = new float[bins];
}

Histogram::~Histogram()
{
    delete [] P;
}

void Histogram::computeLog( const pfs::Array2D *image )
{
    const int size = image->getRows()*image->getCols();

    float max, min;               // Find min, max
    {
        min = 999999999;
        max = -999999999;
        
        for( int i = 0; i < size; i += accuracy )
        {
            float v = (*image)(i);
            
            if( v > max )      max = v;
            else if( v < min ) min = v;
        }
    }
    
    computeLog( image, min, max );
}

void Histogram::computeLog( const pfs::Array2D *image, float min, float max )
{
    const int size = image->getRows()*image->getCols();
    
    // Empty all bins
    for( int i = 0; i < bins; i++ )
        P[i] = 0;

    float count    = 0;
    float binWidth = (max-min)/(float)bins;
    
    for( int i = 0; i < size; i += accuracy )
    {
        float v = (*image)(i);
        
        if( v <= 0 ) continue;
        
        v       = log10(v);
        int bin = (int)((v-min)/binWidth);
        
        if( bin > bins || bin < 0 ) continue;
        
        if( bin == bins ) bin = bins-1;
        
        P[bin] += 1;
        count++;
    }

    // Normalize, to get probability
    for( int i = 0; i < bins; i++ )
        P[i] /= (float)(count/accuracy);
}

float Histogram::getMaxP() const
{
    float maxP = -1;
    for( int i = 0; i < bins; i++ )
    {
        if( P[i] > maxP )
        {
            maxP = P[i];
        }    
    }
    
    return maxP;
}

}  // namespace KIPIHDRCreatorPlugin
