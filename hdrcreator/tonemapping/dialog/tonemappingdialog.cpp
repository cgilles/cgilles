/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-11-13
 * Description : a plugin to make HDR images.
 *               Original implementation from Qtpfsgui.
 *
 * Copyright (C) 2006-2007 by Giuseppe Rota <grota at users dot sourceforge dot net>
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "tonemappingdialog.moc"

// Qt includes

#include <QDockWidget>
#include <QMdiArea>
#include <QMdiSubWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QWhatsThis>
#include <QPointer>

// KDE includes

#include <kapplication.h>
#include <kurl.h>
#include <klocale.h>
#include <kmessagebox.h>
#include <kfiledialog.h>
#include <kimageio.h>
#include <kdebug.h>

// LibKipi inlcudes

#include <libkipi/interface.h>
#include <libkipi/imagecollection.h>

// Local includes

#include "saveldrimgthread.h"
#include "hdrviewer.h"
#include "ldrviewer.h"
#include "options.h"
#include "global.h"
#include "configuration.h"

namespace KIPIHDRCreatorPlugin
{

TonemappingWindow::TonemappingWindow(QWidget *parent, pfs::Frame* frame,
                                     const QString& _file, Interface* iface)
                 : QMainWindow(parent), m_interface(iface)
{
    qRegisterMetaType<TMImg>();
    m_saveThread = new SaveLdrImgThread(this);

    setupUi(this);
    toolBar->setToolButtonStyle((Qt::ToolButtonStyle)s_settings.value(KEY_TOOLBAR_MODE, Qt::ToolButtonTextUnderIcon).toInt());

    prefixname        = QFileInfo(_file).completeBaseName();
    setWindowTitle(windowTitle() + prefixname);
    recentPathSaveLDR = s_settings.value(KEY_RECENT_PATH_SAVE_LDR, QDir::currentPath()).toString();

    mdiArea = new QMdiArea(this);
    mdiArea->setBackground(QBrush(QColor::fromRgb(192, 192, 192)) );
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    setCentralWidget(mdiArea);

    //------------------------------------------------------------------

    QDockWidget *dockl       = new QDockWidget(i18n("HDR Viewer Panel"), this);
    QtpfsguiOptions* options = QtpfsguiOptions::getInstance();
    m_hdrViewver             = new HdrViewer(dockl, frame, options->negcolor, options->naninfcolor);
    dockl->setWidget(m_hdrViewver);
    dockl->setAllowedAreas(Qt::LeftDockWidgetArea);
    dockl->setFeatures(QDockWidget::DockWidgetClosable);

    addDockWidget(Qt::LeftDockWidgetArea, dockl);

    //------------------------------------------------------------------

    QDockWidget *dockr = new QDockWidget(i18n("Tone mapping Panel"), this);
    TMWidget *tmwidget = new TMWidget(dockr, frame, statusBar());
    dockr->setWidget(tmwidget);
    dockr->setAllowedAreas(Qt::RightDockWidgetArea);
    dockr->setFeatures(QDockWidget::DockWidgetClosable);

    addDockWidget(Qt::RightDockWidgetArea, dockr);

    //------------------------------------------------------------------

    connect(mdiArea, SIGNAL(subWindowActivated(QMdiSubWindow*)),
            this, SLOT(updateActions(QMdiSubWindow *)) );

    connect(actionViewTMDock, SIGNAL(toggled(bool)),
            dockr, SLOT(setVisible(bool)));

    connect(actionViewHDRViewerDock, SIGNAL(toggled(bool)),
            dockl, SLOT(setVisible(bool)));

    connect(tmwidget, SIGNAL(signalNewResult(const KIPIHDRCreatorPlugin::TMImg&, tonemapping_options*)),
            this, SLOT(slotAddMDIresult(const KIPIHDRCreatorPlugin::TMImg&, tonemapping_options*)));

    connect(actionAsThumbnails, SIGNAL(triggered()),
            this, SLOT(viewAllAsThumbnails()));

    connect(actionCascade, SIGNAL(triggered()),
            mdiArea, SLOT(cascadeSubWindows()));

    connect(actionFit_to_Window, SIGNAL(toggled(bool)),
            this, SLOT(current_ldr_fit_to_win(bool)));

    connect(actionWhat_s_This, SIGNAL(triggered()),
            this, SLOT(enterWhatsThis()));

    connect(m_saveThread, SIGNAL(signalComplete(const KUrl::List&, const KUrl::List&)),
            this, SLOT(slotThreadDone(const KUrl::List&, const KUrl::List&)));

    showMaximized();
}

TonemappingWindow::~TonemappingWindow()
{
}

void TonemappingWindow::slotAddMDIresult(const TMImg& data, tonemapping_options* opts)
{
    LdrViewer *n = new LdrViewer(this, data, opts);

    mdiArea->addSubWindow(n);
    n->show();
}

void TonemappingWindow::current_ldr_fit_to_win(bool checked)
{
    LdrViewer* currentLDR = ((LdrViewer*)(mdiArea->activeSubWindow()->widget()));
    if (!currentLDR)
        return;

    currentLDR->slotFitToWindow(checked);
}

void TonemappingWindow::updateActions(QMdiSubWindow* w)
{
    actionSave->setEnabled(w != NULL);
    actionSaveAll->setEnabled(w != NULL);
    actionClose_All->setEnabled(w != NULL);
    actionAsThumbnails->setEnabled(w != NULL);
    actionFit_to_Window->setEnabled(w != NULL);
    actionCascade->setEnabled(w != NULL);
    if (w != NULL)
    {
        LdrViewer* current = (LdrViewer*)(mdiArea->activeSubWindow()->widget());
        if (!current)
            return;

        actionFit_to_Window->setChecked(current->getFittingWin());
    }
}

void TonemappingWindow::viewAllAsThumbnails()
{
    mdiArea->tileSubWindows();
    QList<QMdiSubWindow*> allLDRresults = mdiArea->subWindowList();
    foreach (QMdiSubWindow* p,allLDRresults)
    {
        ((LdrViewer*)p->widget())->slotFitToWindow(true);
    }
}

void TonemappingWindow::on_actionClose_All_triggered()
{
    QList<QMdiSubWindow*> allLDRresults = mdiArea->subWindowList();
    foreach (QMdiSubWindow *p,allLDRresults)
    {
        p->close();
    }
}

void TonemappingWindow::enterWhatsThis()
{
    QWhatsThis::enterWhatsThisMode();
}

void TonemappingWindow::on_actionSave_triggered()
{
    LdrViewer* currentLDR = ((LdrViewer*)(mdiArea->activeSubWindow()->widget()));
    if (!currentLDR)
        return;

    saveOneLdrImage(prefixname + QString("_") + currentLDR->getFilenamePostFix() + QString(".png"),
                    currentLDR->getTMImage(),
                    currentLDR->getExifComment());
}

void TonemappingWindow::on_actionSaveAll_triggered()
{
    setCursor(Qt::WaitCursor);
    setEnabled(false);

    KUrl outUrl(QDir::homePath());
    if (m_interface)
        outUrl = m_interface->currentAlbum().uploadPath().path();
    
    QList<QMdiSubWindow*> allLDRresults = mdiArea->subWindowList();
    QMap<KUrl, TMImg>   map;
    QMap<KUrl, QString> com;

    foreach (QMdiSubWindow* p, allLDRresults)
    {
        LdrViewer* ldr = ((LdrViewer*)p->widget());
        KUrl url       = outUrl;
        url.setFileName(prefixname + QString("_") + ldr->getFilenamePostFix() + QString(".png"));
        map.insert(url, ldr->getTMImage());
        com.insert(url, ldr->getExifComment());
    }

    m_saveThread->setImagesMap(map);
    m_saveThread->setFormat(QString("PNG"));
    m_saveThread->setCommentsMap(com);
    m_saveThread->start();
}

void TonemappingWindow::saveOneLdrImage(const QString& initialFileName, const TMImg& image, const QString& comment)
{
    KUrl outUrl(QDir::homePath());
    if (m_interface)
        outUrl = m_interface->currentAlbum().uploadPath().path();

    QStringList writableMimetypes = KImageIO::mimeTypes(KImageIO::Writing);
    // Put first class citizens at first place
    writableMimetypes.removeAll("image/jpeg");
    writableMimetypes.removeAll("image/tiff");
    writableMimetypes.removeAll("image/png");
    writableMimetypes.insert(0, "image/png");
    writableMimetypes.insert(1, "image/jpeg");
    writableMimetypes.insert(2, "image/tiff");

    QString format("PNG");
    QString defaultMimeType("image/png");
    QString defaultFileName(initialFileName);

    QPointer<KFileDialog> imageFileSaveDialog = new KFileDialog(outUrl, QString(), 0);

    imageFileSaveDialog->setModal(false);
    imageFileSaveDialog->setOperationMode(KFileDialog::Saving);
    imageFileSaveDialog->setMode(KFile::File);
    imageFileSaveDialog->setSelection(defaultFileName);
    imageFileSaveDialog->setCaption(i18n("New Image File Name"));
    imageFileSaveDialog->setMimeFilter(writableMimetypes, defaultMimeType);

    // Start dialog and check if canceled.
    if ( imageFileSaveDialog->exec() != KFileDialog::Accepted )
        return;

    KUrl newURL = imageFileSaveDialog->selectedUrl();
    QFileInfo fi(newURL.toLocalFile());

    // Check if target image format have been selected from Combo List of dialog.

    const QStringList mimes = KImageIO::typeForMime(imageFileSaveDialog->currentMimeFilter());
    if (!mimes.isEmpty())
    {
        format = mimes.first().toUpper();
    }
    else
    {
        // Else, check if target image format have been add to target image file name using extension.

        format = fi.suffix().toUpper();

        // Check if format from file name extension is include on file mime type list.

        QStringList imgExtList = KImageIO::types(KImageIO::Writing);
        imgExtList << "TIF";
        imgExtList << "TIFF";
        imgExtList << "JPG";
        imgExtList << "JPE";

        if ( !imgExtList.contains( format ) )
        {
            KMessageBox::error(0, i18n("The target image file format \"%1\" is unsupported.", format));
            kWarning() << "target image file format " << format << " is unsupported!";
            return;
        }
    }

    if (!newURL.isValid())
    {
        KMessageBox::error(0, i18n("Failed to save file\n\"%1\" to\n\"%2\".",
                                newURL.fileName(),
                                newURL.path().section('/', -2, -2)));
        kWarning() << "target URL is not valid !";
        return;
    }

    // Check for overwrite ----------------------------------------------------------

    if ( fi.exists() )
    {
        int result = KMessageBox::warningYesNo(0, i18n("A file named \"%1\" already "
                                                        "exists. Are you sure you want "
                                                        "to overwrite it?",
                                                newURL.fileName()),
                                                i18n("Overwrite File?"),
                                                KStandardGuiItem::overwrite(),
                                                KStandardGuiItem::cancel());

        if (result != KMessageBox::Yes)
            return;
    }

    delete imageFileSaveDialog;

    outUrl = newURL;

    setCursor(Qt::WaitCursor);
    setEnabled(false);

    // Perform saving ---------------------------------------------------------------

    QMap<KUrl, TMImg> img;
    img.insert(outUrl, image);

    QMap<KUrl, QString> com;
    com.insert(outUrl, comment);

    m_saveThread->setFormat(format);
    m_saveThread->setImagesMap(img);
    m_saveThread->setCommentsMap(com);
    m_saveThread->start();
}

void TonemappingWindow::slotThreadDone(const KUrl::List& complete, const KUrl::List& failed)
{
    if (!failed.isEmpty())
        KMessageBox::errorList(0, i18n("Cannot save files listed below:"), failed.toStringList());

    if (m_interface)
        m_interface->refreshImages(complete);

    unsetCursor();
    setEnabled(true);
}

} // namespace KIPIHDRCreatorPlugin
