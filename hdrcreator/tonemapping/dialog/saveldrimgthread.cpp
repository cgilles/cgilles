/* ============================================================
 *
 * This file is a part of kipi-plugins project
 * http://www.kipi-plugins.org
 *
 * Date        : 2009-12-05
 * Description : save LDR image thread
 *
 * Copyright (C) 2009 by Gilles Caulier <caulier dot gilles at gmail dot com>
 *
 * This program is free software; you can redistribute it
 * and/or modify it under the terms of the GNU General
 * Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * ============================================================ */

#include "saveldrimgthread.moc"

// Qt includes

#include <QImage>
#include <QDateTime>

// KDE includes

#include <kdebug.h>

// Local includes

#include "kpmetadata.h"
#include "kpwriteimage.h"
#include "kpversion.h"

using namespace KExiv2Iface;
using namespace KDcrawIface;
using namespace KIPIPlugins;

namespace KIPIHDRCreatorPlugin
{

class SaveLdrImgThreadPriv
{
public:

    SaveLdrImgThreadPriv()
    {
    }

    QMap<KUrl, TMImg>   images;
    QMap<KUrl, QString> comments;

    QString             format;
};

SaveLdrImgThread::SaveLdrImgThread(QObject *parent)
                : QThread(parent), d(new SaveLdrImgThreadPriv)
{
}

SaveLdrImgThread::~SaveLdrImgThread()
{
    // wait for the thread to finish
    wait();

    delete d;
}

void SaveLdrImgThread::setCommentsMap(const QMap<KUrl, QString>& map)
{
    d->comments = map;
}

void SaveLdrImgThread::setImagesMap(const QMap<KUrl, TMImg>& map)
{
    d->images = map;
}

void SaveLdrImgThread::setFormat(const QString& format)
{
    d->format = format;
}

void SaveLdrImgThread::run()
{
    QMap<KUrl, TMImg>::const_iterator it;
    KUrl::List                        completed, failed;

    for ( it = d->images.begin(); it != d->images.end(); ++it )
    {
        if (!saveOneImage(it.key(), it.value(), d->comments[it.key()]))
            failed.append(it.key());
        else
            completed.append(it.key());
    }

    emit signalComplete(completed, failed);
}

bool SaveLdrImgThread::saveOneImage(const KUrl& url, const TMImg& img, const QString& comments)
{
    QImage qimg     = img.toQImage();
    QImage prev     = qimg.scaled(1280, 1024, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    QImage thumb    = qimg.scaled(160, 120,   Qt::KeepAspectRatio, Qt::SmoothTransformation);
    QByteArray prof = KPWriteImage::getICCProfilFromFile(RawDecodingSettings::SRGB);

    KPMetadata meta;

    if (d->format != QString("JPEG"))
        meta.setImagePreview(prev);

    meta.setImageProgramId(QString("Kipi-plugins"), QString(kipiplugins_version));
    meta.setImageDimensions(img.size());
    meta.setExifThumbnail(thumb);
    meta.setExifTagString("Exif.Image.DocumentName", QString("HDR Image")); // not i18n
    meta.setImageDateTime(QDateTime::currentDateTime());
    meta.setImageOrientation(KExiv2::ORIENTATION_NORMAL);
    meta.setImageColorWorkSpace(KExiv2::WORKSPACE_SRGB);
    meta.setComments(comments.toUtf8());
    meta.setExifComment(comments);
    meta.setXmpTagStringLangAlt("Xmp.dc.description", comments, QString(), false);
    meta.setXmpTagStringLangAlt("Xmp.exif.UserComment", comments, QString(), false);
    meta.setXmpTagStringLangAlt("Xmp.tiff.ImageDescription", comments, QString(), false);
    meta.setIptcTagString("Iptc.Application2.Caption", comments);

    KPWriteImage wImageIface;
    wImageIface.setImageData(img.data, img.width, img.height, true, false, prof, meta);

    bool ret = false;

    QString path;
    if(url.isLocalFile())
        path = url.toLocalFile();
    else
        path = url.path();

    if (d->format == QString("JPEG"))
    {
        ret = wImageIface.write2JPEG(path);
    }
    else if (d->format == QString("PNG"))
    {
        ret = wImageIface.write2PNG(path);
    }
    else if (d->format == QString("TIFF"))
    {
        ret = wImageIface.write2TIFF(path);
    }
    else
    {
        ret = qimg.save(path, d->format.toAscii().data());
    }

    return ret;
}

}  // namespace KIPIHDRCreatorPlugin
