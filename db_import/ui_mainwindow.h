#include <kdialog.h>
#include <klocale.h>

/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Tue Jul 6 08:44:37 2010
**      by: Qt User Interface Compiler version 4.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QAction *actionOptions;
    QAction *actionAbout;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QLabel *label_2;
    QLineEdit *digikamDatabaseSqliteFilePath;
    QPushButton *select_digikam_db_file;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QVBoxLayout *verticalLayout_3;
    QLineEdit *lineEdit;
    QPushButton *pushButton_2;
    QWidget *tab_2;
    QWidget *tab_3;
    QWidget *tab_4;
    QPushButton *importButton;
    QLabel *current_action;
    QMenuBar *menuBar;
    QMenu *menuApplication;
    QMenu *menuTools;
    QMenu *menu;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(500, 276);
        MainWindow->setTabShape(QTabWidget::Rounded);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionOptions = new QAction(MainWindow);
        actionOptions->setObjectName(QString::fromUtf8("actionOptions"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy);

        verticalLayout->addWidget(label_2);

        digikamDatabaseSqliteFilePath = new QLineEdit(centralWidget);
        digikamDatabaseSqliteFilePath->setObjectName(QString::fromUtf8("digikamDatabaseSqliteFilePath"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(digikamDatabaseSqliteFilePath->sizePolicy().hasHeightForWidth());
        digikamDatabaseSqliteFilePath->setSizePolicy(sizePolicy1);
#ifndef UI_QT_NO_ACCESSIBILITY
        digikamDatabaseSqliteFilePath->setAccessibleDescription(QString::fromUtf8(""));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout->addWidget(digikamDatabaseSqliteFilePath);

        select_digikam_db_file = new QPushButton(centralWidget);
        select_digikam_db_file->setObjectName(QString::fromUtf8("select_digikam_db_file"));

        verticalLayout->addWidget(select_digikam_db_file);

        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(tabWidget->sizePolicy().hasHeightForWidth());
        tabWidget->setSizePolicy(sizePolicy2);
        tabWidget->setMinimumSize(QSize(450, 0));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        verticalLayout_2 = new QVBoxLayout(tab);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label = new QLabel(tab);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetMaximumSize);
        lineEdit = new QLineEdit(tab);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        sizePolicy1.setHeightForWidth(lineEdit->sizePolicy().hasHeightForWidth());
        lineEdit->setSizePolicy(sizePolicy1);
#ifndef UI_QT_NO_ACCESSIBILITY
        lineEdit->setAccessibleDescription(QString::fromUtf8(""));
#endif // QT_NO_ACCESSIBILITY

        verticalLayout_3->addWidget(lineEdit);

        pushButton_2 = new QPushButton(tab);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(pushButton_2->sizePolicy().hasHeightForWidth());
        pushButton_2->setSizePolicy(sizePolicy3);

        verticalLayout_3->addWidget(pushButton_2);


        verticalLayout_2->addLayout(verticalLayout_3);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        tabWidget->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QString::fromUtf8("tab_3"));
        tabWidget->addTab(tab_3, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        tabWidget->addTab(tab_4, QString());

        verticalLayout->addWidget(tabWidget);

        importButton = new QPushButton(centralWidget);
        importButton->setObjectName(QString::fromUtf8("importButton"));
        importButton->setEnabled(false);

        verticalLayout->addWidget(importButton);

        current_action = new QLabel(centralWidget);
        current_action->setObjectName(QString::fromUtf8("current_action"));

        verticalLayout->addWidget(current_action);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 500, 21));
        menuApplication = new QMenu(menuBar);
        menuApplication->setObjectName(QString::fromUtf8("menuApplication"));
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName(QString::fromUtf8("menuTools"));
        menu = new QMenu(menuBar);
        menu->setObjectName(QString::fromUtf8("menu"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuApplication->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menu->menuAction());
        menuApplication->addAction(actionExit);
        menuTools->addAction(actionOptions);
        menu->addAction(actionAbout);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(tr2i18n("digiKam database import tool", 0));
        actionExit->setText(tr2i18n("Exit", 0));
        actionOptions->setText(tr2i18n("Options", 0));
        actionAbout->setText(tr2i18n("About", 0));
        label_2->setText(tr2i18n("digiKam database path", 0));
        digikamDatabaseSqliteFilePath->setText(QString());
        select_digikam_db_file->setText(tr2i18n("Select file", 0));
        label->setText(tr2i18n("Nikon PictureProject database path", 0));
        lineEdit->setText(QString());
        pushButton_2->setText(tr2i18n("Select folder", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), tr2i18n("Nikon PictureProject", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), tr2i18n("Picasa", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_3), tr2i18n("Live Gallery", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), tr2i18n("Lightroom", 0));
        importButton->setText(tr2i18n("Import", 0));
        current_action->setText(QString());
        menuApplication->setTitle(tr2i18n("Application", 0));
        menuTools->setTitle(tr2i18n("Tools", 0));
        menu->setTitle(tr2i18n("?", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MAINWINDOW_H

